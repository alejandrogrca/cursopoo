package org.uma.mbd.mdMasterMind.masterMind;

public class Movimiento {

    private String cifras;
    private int colocadas;
    private int descolocadas;

    public Movimiento(String ci, int col, int descol) {
        cifras = ci;
        colocadas = col;
        descolocadas  = descol;
    }

    public String getCifras() {
        return cifras;
    }

    public int getColocadas() {
        return colocadas;
    }

    public int getDescolocadas() {
        return descolocadas;
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = obj instanceof Movimiento;
        Movimiento mov = res ? (Movimiento)obj : null;
        return res && cifras == mov.cifras;
    }

    @Override
    public int hashCode() {
        return cifras.hashCode();
    }

    @Override
    public String toString() {
        return "[" + cifras + ", " + colocadas + ", " + descolocadas + "]";
    }
}
