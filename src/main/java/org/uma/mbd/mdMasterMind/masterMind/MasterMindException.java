package org.uma.mbd.mdMasterMind.masterMind;

public class MasterMindException extends Exception {

    public MasterMindException() {
        super();
    }

    public MasterMindException(String msg) {
        super(msg);
    }
}

