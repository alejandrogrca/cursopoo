package org.uma.mbd.mdAlturasCorrespondencias.alturas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Paises {
    private List<Pais> paises;

    public Paises() {
        paises = new LinkedList<>();
    }

    public void leePaises(String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner(new File(file))) {
            leePaises(sc);
        }
    }

    public void leePaises(Scanner sc)  {
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
           paises.add(stringToPais(linea));
        }
    }

    private Pais stringToPais(String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[,]");
            sc.useLocale(Locale.ENGLISH);
            String n = sc.next();
            String c = sc.next();
            double al = sc.nextDouble();
            return new Pais(n,c,al);
        }
    }

    public List<Pais> selecciona(Seleccion sel) {
        List<Pais> sol = new ArrayList<>();
        for(Pais pais : paises) {
            if (sel.test(pais)) {
                sol.add(pais);
            }
        }
        return sol;
    }

    public Map<String, List<Pais>> porContinentes() {
        Map<String, List<Pais>> map = new TreeMap();
        for(Pais pais : paises) {
            String continente = pais.getContinente();
            List<Pais> lista = map.computeIfAbsent(continente, k -> new ArrayList<>());
            lista.add(pais);
        }
        return map;
    }

    public Map<Double, Set<String>> continentesporAltura() {
        Map<Double, Set<String>> map = new TreeMap<>();
        for(Pais pais : paises) {
            double altura = pais.getAltura();
            String continente = pais.getContinente();
            Set<String> set = map.computeIfAbsent(altura, k -> new TreeSet<>());
            set.add(continente);
        }
        return map;
    }
}
