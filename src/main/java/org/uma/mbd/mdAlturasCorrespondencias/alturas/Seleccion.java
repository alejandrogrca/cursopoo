package org.uma.mbd.mdAlturasCorrespondencias.alturas;

public interface Seleccion {
    boolean test(Pais pais);
}
