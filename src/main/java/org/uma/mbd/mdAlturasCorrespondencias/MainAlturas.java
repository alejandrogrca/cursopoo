package org.uma.mbd.mdAlturasCorrespondencias;

import javafx.beans.binding.MapBinding;
import org.uma.mbd.mdAlturasCorrespondencias.alturas.Pais;
import org.uma.mbd.mdAlturasCorrespondencias.alturas.Paises;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainAlturas {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");
        System.out.println("Menores que 1.7");
        for (Pais pais : paises.selecciona(p -> p.getAltura() < 1.7)) {
            System.out.println(pais);
        }
        System.out.println("En europa");
        for (Pais pais : paises.selecciona(p -> p.getContinente().equals("Europe"))) {
            System.out.println(pais);
        }

        System.out.println("Correspondencias");
        Map<String, List<Pais>> map = paises.porContinentes();
        for(Map.Entry<String, List<Pais>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "\t");
            for(Pais pais : entry.getValue()) {
                System.out.println("\t" + pais);
            }
        }

        System.out.println();
        Map<Double, Set<String>> map2 = paises.continentesporAltura();
        for(Map.Entry<Double, Set<String>> entry : map2.entrySet()) {
            System.out.print(entry.getKey());
            for(String continente : entry.getValue()) {
                System.out.println(continente + " ");
            }
            System.out.println();
        }


    }
}
