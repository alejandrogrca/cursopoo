package org.uma.mbd.mdNotas.notas;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Asignatura {
	private String nombre;
	private List<Alumno> alumnos;
	private List<String> errores;

	private static final double APROBADO = 5;
	private static final String DNI = "[0-9]{8}[A-Z&&[^IOU]]";

	public Asignatura(String nombreAsignatura) {
		nombre = nombreAsignatura;
		alumnos = new ArrayList<>();
		errores = new ArrayList<>();
	}

	public void leeDatos(String nombreFichero) throws IOException {
		try (Scanner sc = new Scanner(new File(nombreFichero))){
		    while (sc.hasNextLine()) {
		        stringAAlumno(sc.nextLine());
            }
		}
	}

	public void leeDatos(String[] datos) {
		for (String dato: datos) {
		    stringAAlumno(dato);
        }
	}

	private void stringAAlumno(String linea) {
		try (Scanner sc = new Scanner(linea)) {
			sc.useDelimiter("[;]+");
			sc.useLocale(Locale.ENGLISH);
			String d = sc.next();
			String no = sc.next();
			double nota = sc.nextDouble();
			if (!d.toUpperCase().matches(DNI)) {
				throw new AlumnoException("DNI Incorrecto");
			}
			Alumno al = new Alumno(no, d, nota);
			alumnos.add(al);
		} catch (AlumnoException e) {
			errores.add(e.getMessage() + ": " + linea);
		} catch (InputMismatchException e) {
			errores.add("Nota no numérica" + ": " + linea);
		} catch (NoSuchElementException e) {
			errores.add("Faltan datos" + ": " + linea);
		}
	}

	public double getCalificacion(Alumno al) throws AlumnoException {
        int pos = alumnos.indexOf(al);
        if (pos < 0) {
            throw new AlumnoException("No existe el alumno " + al);
        }
        return alumnos.get(pos).getCalificacion();
    }

	public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public List<String> getErrores() {
		return errores;
	}

	public double getMedia(CalculoMedia media) throws AlumnoException {
		return media.calcular(getAlumnos());
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nombre);
		sb.append(alumnos);
		sb.append(errores);
		return sb.toString();
	}
}
