package org.uma.mbd.mdNotas.notas;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;

public class MediaSinExtremos implements CalculoMedia{
	private double min;
	private double max;

	public MediaSinExtremos(double min, double max){
		this.min = min;
		this.max = max;
	}

	@Override
	public double calcular(List<Alumno> als) throws AlumnoException{
		double sum = 0;
		int na = 0;
		for (Alumno alumno : als) {
			double cal = alumno.getCalificacion();
			if (cal >= min && cal <= max) {
				sum += alumno.getCalificacion();
				na++;
			}
		}
		if (na == 0) throw new AlumnoException("No hay alumnos para calcular la media");
		return sum / na;
	}
}
