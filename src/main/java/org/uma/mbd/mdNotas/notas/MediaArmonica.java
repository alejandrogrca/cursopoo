package org.uma.mbd.mdNotas.notas;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;

public class MediaArmonica implements CalculoMedia{

	@Override
	public double calcular(List<Alumno> als) throws AlumnoException {
		double sum = 0;
		int na = 0;
		for (Alumno alumno : als) {
			if (alumno.getCalificacion() != 0) {
				sum += alumno.getCalificacion();
				na++;
			}
		}
		if (na == 0) throw new AlumnoException("No hay alumnos para calcular la media");
		return sum / na;
	}
}
