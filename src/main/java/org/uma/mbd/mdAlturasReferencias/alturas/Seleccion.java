package org.uma.mbd.mdAlturasReferencias.alturas;

public interface Seleccion {
    boolean test(Pais pais);
}
