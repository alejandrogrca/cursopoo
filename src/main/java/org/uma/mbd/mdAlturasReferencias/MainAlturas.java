package org.uma.mbd.mdAlturasReferencias;

import org.uma.mbd.mdAlturasReferencias.alturas.*;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class MainAlturas {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");
        System.out.println("Menores que 1.7");

        // Bloque 1: Ordenamos por continente
        Predicate<Pais> pred = p -> p.getAltura() < 1.7;
        List<Pais> lista = paises.selecciona(pred);
        Comparator<Pais> comp = new OrdenContinente();
        //Comparator<Pais> comp = new OrdenContinente().reversed(); //Si quisiera ordenar por continente en orden contrario.
        //Comparator<Pais> comp = new OrdenContinente().thenComparing(new OrdenNombre); // Si quisiera ordenar por continente y luego por nombre de países.

        // Bloque 2: Ordenamos por continente, por altura inversa y por nombre inverso.
        Comparator<Pais> oNombre = new OrdenNombre();
        Comparator<Pais> oContinente = new OrdenContinente();
        Comparator<Pais> oAltura = new OrdenAltura();
        Comparator<Pais> comp1 = oContinente.thenComparing(oAltura.reversed()).thenComparing(oNombre.reversed());

        Predicate<Pais> pred2 = p -> p.getAltura() < 1.7;
        List<Pais> lista2 = paises.selecciona(pred);
        Comparator<Pais> comp2 = new OrdenNombre().thenComparing(Comparator.<Pais>naturalOrder()); // Compara por nombre y en caso de ser iguales igual compara por el orden natural.

        Set<Pais> sp = new TreeSet<>(lista);

        Set<Pais> spa = new TreeSet<>(oContinente); // Conjunto de paises por continente
        spa.addAll(lista); // Añadimos toda la lista de paises

        for (Pais pais : paises.selecciona(p -> p.getAltura() < 1.7)) {
            System.out.println(pais);
        }

        System.out.println("En europa");

        Predicate<Pais> pred3 = p -> p.getAltura() < 1.7;
        List<Pais> lista3 = paises.selecciona(pred);
        Comparator<Pais> comp3 = new OrdenNombre();
        lista.sort(comp2);
        for (Pais pais : paises.selecciona(p -> p.getContinente().equals("Europe"))) {
            System.out.println(pais);
        }

        System.out.println("Conjuntos");
        System.out.println(paises.getContinentes()); // Continentes ordenados de A-Z.
        System.out.println(paises.getAlturas()); // Alturas ordenadas de menor a mayor.

        /*Map<String, List<Pais>> ppc = paises.getPaises().steram().collect(Collectors.group)*/
    }
}
