package org.uma.mbd.mdBuses;

import org.uma.mbd.mdBuses.buses.Criterio;
import org.uma.mbd.mdBuses.buses.EnMatricula;
import org.uma.mbd.mdBuses.buses.PorLinea;
import org.uma.mbd.mdBuses.buses.Servicio;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MainBuses {
    public static void main(String[] args) {

        Servicio servicio = new Servicio("Malaga");

        try {
            System.out.println(servicio.getCiudad());
            servicio.leeBuses("recursos/mdBus/buses.txt");

            PrintWriter pw = new PrintWriter(System.out, true);

            Criterio cr1 = new PorLinea(21);
            servicio.guarda("recursos/mdBus/linea21.txt", cr1);
            servicio.guarda(pw, cr1);

            Criterio cr2 = new EnMatricula("29");
            servicio.guarda("recursos/mdBus/contiene29.txt", cr2);
            servicio.guarda(pw, cr2);

            servicio.guarda(new PrintWriter(System.out, true),
                    bus -> bus.getMatricula().contains("29"));

            Criterio cr3 = bus -> bus.getMatricula().startsWith("2");
            servicio.guarda(pw, cr3);

        } catch (FileNotFoundException e) {
            System.err.println("No existe el fichero de entrada");
        } catch (NullPointerException e) {
            System.err.println("Error");
        }
    }
}
