package org.uma.mbd.mdBuses.buses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Servicio {

    // Variables de clase
    private String ciudad;
    private Bus[] buses;
    private int TAM = 10;

    public Servicio(String ci) {
        ciudad = ci;
        buses = new Bus[TAM];
    }

    public String getCiudad() {
        return ciudad;
    }

    public Bus[] getBuses() {
        return buses;
    }

    public void leeBuses(String file) throws FileNotFoundException {
        String fichero = "recursos/mdBus/buses.txt";
        // Otra forma de acceder al fichero sería:
        // String fichero = "C:\\Users\\Usuario\\IdeaProjects\\CursoPOO\\recursos\\mdBus";
        try(Scanner sc = new Scanner (new File(fichero))) {
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());
            }
        }
    }

    private void leeBuses(Scanner sc) {
        int pos = 0;
        while(sc.hasNextLine()) {
            String linea = sc.nextLine();
            try {
                Bus bus = stringToBus(linea);
                if (pos == buses.length) {
                    buses = Arrays.copyOf(buses, buses.length * 2);
                }
                buses[pos] = bus;
                pos++;
            } catch (InputMismatchException e) {
                System.err.println("ERROR: dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("ERROR: faltan datos en " + linea);
            }
        }
        buses = Arrays.copyOf(buses, pos);
    }

    private Bus stringToBus(String linea) {
        try(Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[:+]");
            int cb = sc.nextInt();
            String mat = sc.next();
            int cl = sc.nextInt();
            Bus bus = new Bus(cb, mat);
            bus.setCodLinea(cb);
            return bus;
        }
    }

    public Bus[] filtra(Criterio ctr) {
        Bus[] sol = new Bus[buses.length];
        int pos = 0;
        for (Bus bus : buses) {
            if(ctr.esSeleccionable(bus)) {
                sol[pos] = bus;
                pos++;
            }
        }
        return Arrays.copyOf(sol, pos);
    }

    public void guarda(String file, Criterio ctr) throws FileNotFoundException { // Este método guarda, nos crea un fichero.
        try (PrintWriter pw = new PrintWriter(file)) {
            guarda(pw, ctr);
        }
    }

    public void guarda(PrintWriter pw, Criterio ctr) { // Este método guarda, nos imprmime por pantalla
        Bus[] cumplen = filtra(ctr);
        pw.println(ctr);
        for(Bus bus : cumplen) {
            pw.println(bus);
        }
    }
}
