package org.uma.mbd.mdBuses.buses;

public interface Criterio {
    boolean esSeleccionable(Bus bus);
}
