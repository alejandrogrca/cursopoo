package org.uma.mbd.mdBusSegundaParte.buses;

public class Bus {

    // Variables de clase
    private String matricula;
    private int codBus;
    private int codLinea;

    public Bus(int cb, String mat) {
        codBus = cb;
        matricula = mat;
    }

    public void setCodLinea(int codLinea) {
        codLinea = codLinea;
    }

    public int getCodBus() {
        return codBus;
    }

    public String getMatricula() {
        return matricula;
    }

    public int getCodLinea() {
        return codLinea;
    }

    @Override
    public boolean equals (Object obj) {
        boolean res = obj instanceof Bus;
        Bus bus = res ? (Bus) obj : null;
        return res && codBus == bus.codBus && matricula.equalsIgnoreCase(bus.matricula);
    }

    @Override
    public int hashCode() {
        return matricula.toUpperCase().hashCode() + Integer.hashCode(codBus);
    }

    @Override
    public String toString() {
        return "Bus (" + codBus + "," + matricula + "," + codLinea + ")";
    }
}
