package org.uma.mbd.mdBusSegundaParte.buses;

public interface Criterio {
    boolean esSeleccionable(Bus bus);
}
