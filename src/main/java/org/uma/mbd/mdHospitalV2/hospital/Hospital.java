package org.uma.mbd.mdHospitalV2.hospital;

public class Hospital {
		private String nombre;
		private String direccion;
		private Departamento [] departamentos;
		private Planta [] plantas;

	public Hospital(String nombre, String direccion, Departamento[] departamentos, int np) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.departamentos = departamentos;
		plantas = new Planta[np];
		for (int i = 0; i < np ; i++) {
			plantas[i] = new Planta(8, "P" + i);
		}
	}

	public String getNombre() {
		return nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public int getNumDepartamentos() {
		return departamentos.length;
	}
		
	public Departamento getDepartamento(String nom) {
		Departamento dep = null;
		int i = 0;
		while (i < departamentos.length && dep == null) {
			if (departamentos[i].getNombre().equals(nom)) {
				dep = departamentos[i];
			}
			i++;
		}
		return dep;
	}
	
	public String [] getNombresDepartamentos() {
		String [] nombres = new String [departamentos.length];
		for (int i = 0; i < departamentos.length; i++) {
			nombres[i] = departamentos[i].getNombre();
		}
		return nombres;
	}
	
	public int numPlantas() {
		return plantas.length;
	}
	
	public Planta getPlanta(int i) {
		if (i < 0 || i >= plantas.length) {
			throw new RuntimeException("Planta inexistente");
		}
		return plantas[i];
		
	}
	
	public String toString() {
		String salida = nombre + " : ";
		for (int i = 0 ; i< departamentos.length; i++) {
			salida += departamentos[i].getNombre() + " ";
		}
		return salida;
	}

}
