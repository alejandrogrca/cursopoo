package org.uma.mbd.mdHospitalV2.hospital;

public class Habitacion {
	private Cama [] camas;
	private String codigo;
	
	/**
	 * Crea una habitaci�n con n camas de codigo dado
	 * @param n    n�mero de camas
	 * @param nh   codigo de la habitaci�n
	 */
	public Habitacion(int n, String nh) { 
		camas = new Cama[n];
		codigo = nh;
		for (int i = 0 ; i < n ; i ++) {
			camas[i] = new Cama(nh + "C" + i);
		}
	}
	
	/**
	 * 
	 * @param i
	 * @return Devuelve la cama de la posici�n i
	 * Si no hay cama lanza una excepci�n
	 */
	public Cama getCama(int i) {
		if (i < 0 || i >= camas.length) {
			throw new RuntimeException("Cama inexistente");
		}
		return camas[i];
	}
	
	/**
	 * 
	 * @return n�mero de camas en la habitaci�n
	 */
	public int numCamas() {
		return camas.length;
	}
	
	/**
	 * 
	 * @return si hay cama libre en la habitaci�n
	 */
	public boolean hayCamaLibre() {
		boolean camaLibre = false;
		int i = 0;
		while (i < camas.length && !camaLibre) {
			camaLibre = camas[i].estaLibre();
			i++;
		}
		return camaLibre;
	}
	
	/**
	 * 
	 * @return una cama libre
	 * Si no hay cama lanza una RuntimeException
	 */
	public Cama camaLibre() {
		Cama camaLibre = null;
		int i = 0;
		while (i < camas.length && camaLibre == null) {
			if (camas[i].estaLibre()) {
				camaLibre = camas[i];
			}
			i++;
		}
		if (camaLibre == null) {
			throw new RuntimeException("No hay camas libres en esta habitacion");
		}
		return camaLibre;
	}

	/**
	 * 
	 * @return el c�digo de habitaci�n
	 */
	public String getCodigo() {
		return codigo;
	}
	
	public String toString() {
		String salida = "Hab("+ codigo + ", ";
		for (Cama c : camas) {
			salida += c + " ";
		}
		salida += ")";
		return salida;
	}
}
