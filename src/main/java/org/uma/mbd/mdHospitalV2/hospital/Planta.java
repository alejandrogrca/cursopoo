package org.uma.mbd.mdHospitalV2.hospital;

public class Planta {
	private Habitacion [] habitaciones;
	private String codigo;
	
	/**
	 * Crea una planta con un n�mero de habitaciones dado.
	 * Todas las habitaciones se crean con 4 camas.
	 * @param n	  Numero de habitaciones de la planta
	 * @param np  Codigo de la planta
	 */
	public Planta(int n, String np) { 
		habitaciones = new Habitacion[n];
		codigo = np;
		for (int i = 0; i < n ; i++) {
			habitaciones[i] = new Habitacion(4, np + "H" + i);
		}
	}
	
	/**
	 * Devuelve la habitaci�n de n�mero i
	 * @param i		N�mero de la habitaci�n
	 * @return		La habitaci�n correspondiente
	 * Lanza una RuntimeException si la habitaci�n no es v�lida
	 */
	public Habitacion getHabitacion(int i) {
		if (i < 0 || i >= habitaciones.length) {
			throw new RuntimeException("Habitacion inexistente");
		}
		return habitaciones[i];
	}
	
	/** 
	 * 
	 * @return Devuelve el n�mero de habitaciones en esta planta
	 */
	public int numHabitaciones() {
		return habitaciones.length;
	}
	
	/**
	 * 
	 * @return Devuelve si hay cama libre en la planta
	 */
	public boolean hayCamaLibre() {
		boolean camaLibre = false;
		int i = 0;
		while (i < habitaciones.length && !camaLibre) {
			camaLibre = habitaciones[i].hayCamaLibre();
			i++;
		}
		return camaLibre;
	}
	
	/**
	 * devuelve una cama libre en la planta
	 * @return la cama libre
	 * Si no hay cama libre lanza una RuntimeException
	 */
	public Cama camaLibre() {
		Cama camaLibre = null;
		int i = 0;
		while (i < habitaciones.length && camaLibre == null) {
			if (habitaciones[i].hayCamaLibre()) {
				camaLibre = habitaciones[i].camaLibre();
			}
			i++;
		}
		if (camaLibre == null) {
			throw new RuntimeException("No hay camas libres en esta planta");
		}
		return camaLibre;
	}

	/**
	 * 
	 * @return  El codigo de la habitaci�n
	 */
	public String getCodigo() {
		return codigo;
	}
	
	public String toString() {
		String salida = "Planta(" + codigo + ", ";
		for (Habitacion c : habitaciones) {
			salida += c + " ";
		}
		salida += ")";
		return salida;
	}
}
