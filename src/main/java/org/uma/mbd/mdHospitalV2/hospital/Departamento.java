package org.uma.mbd.mdHospitalV2.hospital;

public class Departamento {
	private String nombre;
	private Medico [] medicos;
	
	public Departamento(String nombre, Medico [] medicos) {
		this.medicos = medicos;
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
		
	public int getNumMedicos() {
		return medicos.length;
	}
		
	public Medico getMedico(String dni) {
		Medico med = null;
		int i = 0;
		while (i < medicos.length && med == null) {
			if (medicos[i].getDni().equals(dni)) {
				med = medicos[i];
			}
			i++;
		}
		return med;
	}

	public String [] getDNIsMedicos() {
		String [] dnis = new String[medicos.length];
		for (int i = 0; i < medicos.length; i++) {
			dnis[i] = medicos[i].getDni();
		}
		return dnis;
	}

	public boolean trabajaEnDepartamento(Medico medico) {
		boolean esta = false;
		int i = 0;
		while ( i < medicos.length && !esta) {
			esta = medicos[i].equals(medico);
			i++;
		}
		return esta;
	}
	
	public int numMedicos(Categoria cat) {
		int enEsp = 0;
		for (int i = 0; i < medicos.length; i++) {
			if (medicos[i].getCategoriaProfesional() == cat) {
				enEsp++;
			}
		}
		return enEsp;
	}
	
	public String toString() {
		String salida = nombre+ " ";
		for (int i = 0; i < medicos.length; i++) {
			salida += medicos[i] + " ";
		}
		return salida;
	}
}
