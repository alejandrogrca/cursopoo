package org.uma.mbd.mdHospitalV2.hospital;
public class Medico extends Persona {
	private Categoria categoriaProf;
	private boolean trabajaEnPrivado;
	private double horasSemanales;

	public Medico(String dni, String nombre, String apellidos, int edad,
			Genero sexo, Categoria cat, boolean trabajaP,
			double horas) {
		super(dni, nombre, apellidos, edad, sexo);
		categoriaProf = cat;
		trabajaEnPrivado = trabajaP;
		horasSemanales = horas;
	}

	public Categoria getCategoriaProfesional() {
		return categoriaProf;
	}

	public void setCategoriaProfesional(Categoria cat) {
		categoriaProf = cat;
	}

	public boolean trabajaEnPrivado() {
		return trabajaEnPrivado;
	}

	public double getHorasSemanales() {
		return horasSemanales;
	}
	
	public String toString() {
		return super.toString() + " - " + getCategoriaProfesional() + "-";
	}
}