package org.uma.mbd.mdHospitalV2.hospital;

public class Cama {
	private Paciente paciente;
	private String codigo;
	
	public Cama(String nc) {
		codigo = nc;
	}
		
	/**
	 * Cambia el paciente
	 * @param p nuevo paciente
	 */
	public void setPaciente(Paciente p) {
		if (paciente == null) throw new RuntimeException("Cama ocupada");
		paciente = p;
		paciente.setCama(this);
	}
	
	/**
	 * 
	 * @return paciente
	 */
	public Paciente getPaciente() {
		return paciente;
	}
	
	/**
	 * 
	 * @return si la habitaci�n est� libre
	 */
	public boolean estaLibre() {
		return paciente == null;
	}
	
	/**
	 * Libera la cama
	 */
	public void libera() {
		paciente = null;
	}
	
	/**
	 *
	 * @return el codigo de la habitaci�n
	 */
	public String getCodigo() {
		return codigo;
	}
	
	public String toString() {
		return "Cama(" + codigo + (paciente!= null?","+paciente.getNombre():"") +")";
	}
}
