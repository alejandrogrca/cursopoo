package org.uma.mbd.mdHospitalV2.hospital;
abstract public class Persona { 
	private String dni;
	private String nombre; 
	private String apellidos; 
	private int edad; 
	private Genero sexo;
	
	public Persona(String dni, String nombre, String apellidos, int edad, Genero sexo) {
		this.dni = dni; 
		this.nombre = nombre; 
		this.apellidos = apellidos; 
		this.edad = edad; 
		this.sexo = sexo;
	}
	
	public String getDni() {
		return dni; 
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getApellidos() { 
		return apellidos;
	}
	
	public int getEdad() { 
		return edad;
	}
	
	public void setEdad(int e) {
		edad = e; 
	} 
	
	public Genero getSexo() {
		return sexo; 
	}
	
	public void setSexo(Genero c) {
		sexo = c; 
	}
	
	public String toString() {
		return dni + " - " + apellidos + ", " + nombre;
	}
}