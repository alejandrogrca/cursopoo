package org.uma.mbd.mdHospitalV2.hospital;
public class Paciente extends Persona {
	private double altura;
	private double peso;
	private String segSocial;
	private boolean urgente;
	private Medico medico;
	private Cama cama;

	public Paciente(String dni, String nombre, String apellidos, int edad,
			Genero sexo, double a, double p, String ss, boolean urg, Cama cam) {
		super(dni, nombre, apellidos, edad, sexo);
		altura = a;
		peso = p;
		segSocial = ss;
		urgente = urg;
		cama = cam;
		cam.setPaciente(this);
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double a) {
		altura = a;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double p) {
		peso = p;
	}

	public double getIndiceMasaCorporal() {
		return getPeso() / Math.pow(getAltura(), 2);
	}

	public String getNumSegSocial() {
		return segSocial;
	}

	public boolean esUrgencia() {
		return urgente;
	}

	public void setEsUrgencia(boolean b) {
		urgente = b;
	}

	public void asignaMedico( Medico med)
	{
		medico = med;
	}
	
	public Medico atendidoPor() {
		return medico;
	}
	
	/**
	 * 
	 * @return devuelve la cama del paciente
	 */
	public Cama getCama() {
		return cama;
	}
	
	/**
	 * Cambia al paciente de cama. Si tiene otra la libera
	 */
	public void setCama(Cama c) {
		cama = c;
	}
	
	/**
	 * Da de alta un paciente.
	 * si tiene ocupada una cama la libera
	 */
	public void daDeAlta() {
		if (cama !=null) {
			cama.libera();
			cama = null;
		}
	}
	
	public String toString() {
		return super.toString() + " (" + getIndiceMasaCorporal() + ", " + cama.getCodigo() +")";
	}
}
