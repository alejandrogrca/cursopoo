package org.uma.mbd.mdJarras.jarras;

/**
 * Clase Jarra
 * @author AlejandroGrca
 */
public class Jarra {

    // Variables de clase.
    private int contenido;
    private final int capacidad;

    /**
     * Método constructor que establece la capacidad de los objetos de la clase Jarra.
     * @param capacidadInicial Parámetro para establecer la capacidad inicial del objeto jarra.
     */
    public Jarra(int capacidadInicial) {
        if (capacidadInicial <= 0) {
            throw new RuntimeException("Capacidad inicial errónea");
        }
        capacidad = capacidadInicial;
        contenido = 0;
    }

    /**
     * Método getter que nos dice la capacidad de un objeto de la clase Jarra.
     * @return Nos devuelve la capacidad.
     */
    public int getCapacidad() {
        return capacidad;
    }

    /**
     * Método getter que nos dice el contenido de un objeto de la clase Jarra.
     * @return Nos devuelve el contenido.
     */
    public int getContenido() {
        return contenido;
    }

    /**
     * Método setter que llena el contenido de un objeto de la clase Jarra.
     */
    public void llena() {
        contenido = capacidad;
    }

    /**
     * Método setter que vacia el contenido de un objeto de la clase Jarra.
     */
    public void vacia() {
        contenido = 0;
    }

    /**
     * Método setter para llenar un objeto de la clase Jarra desde otro objeto de la misma clase.
     */
    public void llenaDesde(Jarra j) {
        int cabe = capacidad - contenido;
        if (cabe >= j.contenido) {
            contenido += j.contenido;
            j.contenido = 0;
        } else {
            contenido = capacidad;
            j.contenido -= cabe;
        }
    }

    /**
     * Método toString que nos da información de la capacidad y el contenido de una jarra.
     * @return Devuelve capacidad y contenido en el formato establecido.
     */
    @Override
    public String toString() {
        return "J(" + capacidad + ", " + contenido + ")";
    }
}
