package org.uma.mbd.mdJarras.jarras;

/**
 * Clase Mesa
 * @author AlejandroGrca
 */
public class Mesa {

    // Variables de clase.
    private Jarra jarraA, jarraB;

    /**
     * Método constructor que establece la capacidad de los objetos de la clase Mesa.
     * @param cA Parámetro para establecer la capacidad de la jarra A.
     * @param cB Parámetro para establecer la capacidad de la jarra B.
     */
    public Mesa(int cA, int cB) {
        jarraA = new Jarra(cA);
        jarraB = new Jarra(cB);
    }

    /**
     * Método setter para llenar la jarra A.
     */
    public void llenaA() {
        jarraA.llena();
    }

    /**
     * Método setter para llenar la jarra B.
     */
    public void llenaB() {
        jarraB.llena();
    }

    /**
     * Método setter para vaciar la jarra A.
     */
    public void vaciaA() {
        jarraA.vacia();
    }

    /**
     * Método setter para vaciar la jarra B.
     */
    public void vaciaB() {
        jarraB.vacia();
    }

    /**
     * Método setter para llenar la jarra B desde la jarra A.
     */
    public void vuelcaASobreB() {
        jarraB.llenaDesde(jarraA);
    }

    /**
     * Método setter para llenar la jarra A desde la jarra B.
     */
    public void vuelvaBSobreA() {
        jarraA.llenaDesde(jarraB);
    }

    /**
     * Método getter que nos dice el contenido de la jarra A.
     * @return Nos devuelve el contenido.
     */
    public int getContenidoA() {
        return jarraA.getContenido();
    }

    /**
     * Método getter que nos dice el contenido de la jarra B.
     * @return Nos devuelve el contenido.
     */
    public int getContenidoB() {
        return jarraB.getContenido();
    }

    /**
     * Método getter que nos dice la capacidad de la jarra A.
     * @return Nos devuelve la capacidad.
     */
    public int getCapacidadA() {
        return jarraA.getCapacidad();
    }

    /**
     * Método getter que nos dice la capacidad de la jarra B.
     * @return Nos devuelve la capacidad.
     */
    public int getCapacidadB() {
        return jarraB.getCapacidad();
    }

    /**
     * Método getter que nos el contenido de la jarra A y jarra B sumados.
     * @return Nos devuelve la suma de los contenidos de A y B.
     */
    public int getContenido() {
        return getContenidoA() + getContenidoB();
    }

    /**
     * Método toString que nos da información de la capacidad y el contenido de un objeto de la clase Jarra.
     * @return Devuelve capacidad y contenido en el formato establecido.
     */
    @Override
    public String toString() {
        return "Mesa(" + jarraA + ", " + jarraB + ")";
    }
}
