package org.uma.mbd.mdJarras;

import org.uma.mbd.mdJarras.jarras.Jarra;
import org.uma.mbd.mdJarras.jarras.Mesa;

/**
 * Método Main
 * @author AlejandroGrca
 */
public class MainJarras {
    public static void main(String [] args) {

        Jarra ja = new Jarra(7);
        Jarra jb = new Jarra(5);

        System.out.println("------- CLASE JARRA -------");

        // Valores iniciales de las capacidades.
        System.out.println("Valores iniciales de las capacidades: ");
        System.out.println("A " + ja);
        System.out.println("B " + jb);

        // Realizamos operaciones para dejar en una de las jarra 1 litro de agua.
        ja.llena();
        jb.llenaDesde(ja);
        jb.vacia();
        jb.llenaDesde(ja);
        ja.llena();
        jb.llenaDesde(ja);
        jb.vacia();
        jb.llenaDesde(ja);
        ja.llena();
        jb.llenaDesde(ja);
        jb.vacia();
        jb.llenaDesde(ja);

        System.out.println("\nResultado del ejercicio: ");
        System.out.println("A " + ja);
        System.out.println("B " + jb);
        System.out.println("\nConclusión: La jarra A es la finalmente se queda con un contenido de 1 litro.");

        System.out.println(); // Separación entre operaciones de la clase Jarra y la clase Mesa.

        // Hacemos lo mismo pero usando la clase Mesa.
        Mesa mesa = new Mesa (7,5);

        System.out.println("------- CLASE MESA -------");

        // Valores iniciales de las capacidades.
        System.out.println("Valores iniciales de las capacidades: ");
        System.out.println("JA :" + mesa.getCapacidadA() + ", JB: " + mesa.getCapacidadB());

        // Realizamos operaciones para dejar en una de las jarra 1 libtro de agua.
        mesa.llenaA();
        mesa.vuelcaASobreB();
        mesa.vaciaB();
        mesa.vuelcaASobreB();
        mesa.llenaA();
        mesa.vuelcaASobreB();
        mesa.vaciaB();
        mesa.vuelcaASobreB();
        mesa.llenaA();
        mesa.vuelcaASobreB();
        mesa.vaciaB();
        mesa.vuelcaASobreB();

        System.out.println("\nResultado del ejercicio: ");
        System.out.println("JA :" + mesa.getContenidoA() + ", JB: " + mesa.getContenidoB());
        System.out.println("\nConclusión: La jarra A es la finalmente se queda con un contenido de 1 litro.");
    }
}
