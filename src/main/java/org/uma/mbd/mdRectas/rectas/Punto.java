package org.uma.mbd.mdRectas.rectas;

public class Punto {
	
	private double x;
	private double y;
	
	public Punto(){
		this(0,0);
	}
	
	public Punto(double a, double b){
		x = a;
		y = b;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public double distancia(Punto p){
		return Math.sqrt(Math.pow((p.getX() - this.getX()),2) +
						       Math.pow((p.getY() - this.getY()),2));
	}
	
	public void setX(double a){
		x = a;
	}
	
	public void setY(double b){
		y = b;
	}
	
	public void trasladar(double a, double b){
		x = x + a;
		y = y + b;
	}

	@Override
	public String toString(){
		return ("P(" + getX() + ", " + getY() + ")");
	}
}