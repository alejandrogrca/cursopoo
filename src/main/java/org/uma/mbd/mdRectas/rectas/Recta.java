package org.uma.mbd.mdRectas.rectas;

/**
 * Created by pacog on 21/6/16.
 */
public class Recta {
    private Vector direccion;
    private Punto pto;

    public Recta(Vector v, Punto p) {
        direccion = v;
        pto = p;
    }

    public Recta(Punto p1, Punto p2) {
        this(new Vector(p1,p2), p2);
    }

    public boolean pasaPor(Punto p) {
        Vector v = new Vector(pto, p);
        return v.paraleloA(direccion);
    }

    public boolean paralelaA(Recta r) {
        return direccion.paraleloA(r.direccion);
    }

    public Recta paralelaPor(Punto p) {
        return new Recta(direccion, p);
    }

    public Recta perpendicularPor(Punto p) {
        return new Recta(direccion.ortogonal(), p);
    }

    public Punto interseccionCon(Recta r) {
        if (this.paralelaA(r)) {
            throw new RuntimeException("Rectas paralelas!!!");
        }
        double d = r.direccion.getComponenteY() * direccion.getComponenteX()
                     - direccion.getComponenteY() * r.direccion.getComponenteX();
        double d1 = direccion.getComponenteX() * pto.getY()
                     - direccion.getComponenteY() * pto.getX();
        double d2 = r.direccion.getComponenteX() * r.pto.getY()
                     - r.direccion.getComponenteY() * r.pto.getX();
        double ox = (d1 * r.direccion.getComponenteX() - d2 * direccion.getComponenteX())/d;
        double oy = (d1 * r.direccion.getComponenteY() - d2 * direccion.getComponenteY())/d;
        return new Punto(ox, oy);
    }

    public double distanciaDesde(Punto p) {
        Recta per = this.perpendicularPor(p);
        Punto pi = this.interseccionCon(per);
        return pi.distancia(p);
    }

    @Override
    public String toString() {
        return "R("+ direccion + ", " + pto +")";
    }
}
