package org.uma.mbd.mdRectas.rectas;

/**
 * Created by pacog on 21/6/16.
 */
public class Vector {
    private Punto extremo;

    public Vector(double x, double y) {
        extremo = new Punto(x,y);
    }

    public Vector(Punto pto) {
        extremo = pto;
    }

    public Vector(Punto p1, Punto p2) {
        extremo = new Punto(p2.getX()-p1.getX(), p2.getY()-p1.getY());
    }

    public double getComponenteX() {
        return extremo.getX();
    }

    public double getComponenteY() {
        return extremo.getY();
    }

    public double modulo() {
        return extremo.distancia(new Punto());
    }

    public Vector ortogonal() {
        return new Vector(-getComponenteY(), getComponenteX());
    }

    public boolean paraleloA(Vector v) {
        return getComponenteX()*v.getComponenteY() == getComponenteY()*v.getComponenteX();
    }

    public Punto extremoDesde(Punto p) {
        return new Punto(p.getX()+ getComponenteX(), p.getY() + getComponenteY());
    }

    @Override
    public String toString() {
        return "V("+ getComponenteX() + ", " + getComponenteY() + ")";
    }

}






