package org.uma.mbd.mdLibreriaV1.libreria;

import java.util.Arrays;

/**
 * Clase Libreria
 * @author AlejandroGrca
 */
public class Libreria {

    // Variables de clase.
    private Libro[] libros; // Array de la clase Libro.
    private int numLibros;
    private static final int TAM_DEFECTO = 16;

    /**
     * Método constructor que establece el tamaño por defecto de la librería.
     */
    public Libreria() {
        this(TAM_DEFECTO);
    }

    /**
     * Método constructor al que le pasamos un tamaño al array Libro.
     * @param tam Parámetro que establece el tamaño de la librería.
     */
    public Libreria(int tam) {
        libros = new Libro[tam];
        numLibros = 0;
    }

    /**
     * Método setter para añadir un libro al array libro pasándole autor, título y precio base.
     */
    public void addLibro(String autor, String titulo, double pb) {
        Libro libro = new Libro(autor, titulo, pb);
        addLibro(libro);
    }

    /**
     * Método setter para añadir un libro pasándole un objeto de la clase Libro.
     */
    private void addLibro(Libro libro) {
        int pos = posicionLibro(libro.getAutor(), libro.getTitulo());
        if (pos >= 0) {
            libros[pos] = libro;
        } else {
            aseguraQueCabe();
            libros[numLibros] = libro;
            numLibros++;
        }
    }

    /**
     * Método setter que comprueba si el libro añadido cabe en el array libros, de no ser así, aumenta el tamaño del
     * array libros en el doble.
     */
    private void aseguraQueCabe() {
        if (numLibros == libros.length) {
            libros = Arrays.copyOf(libros, libros.length*2);
        }
    }

    /**
     * Método getter que nos dice la posición de un libro en el array libros, dados el nombre del autor y el título. Este
     * método se crea para poder usarlo en el método remLibro().
     * @return Nos devuelve -1 si el libro no lo encuentra (i es igual al número de libros, lo que significa que el array
     * ha llegado a su fín sin encontrar el libro) o nos devuelve i (posición del array en la que encuentra el libro).
     */
    private int posicionLibro(String autor, String titulo) {
        int i = 0;
        while (i < numLibros &&
                  !(autor.equalsIgnoreCase(libros[i].getAutor())
                    && titulo.equalsIgnoreCase(libros[i].getTitulo()))) {
            i++;
        }
        return (i == numLibros) ? -1 : i; // Si i = número de libros devuelve -1 sino devuelve i.
    }

    /**
     * Método setter para eliminar un libro del array en el caso de que esté repetido. Al eliminarlo hace que todos los libros
     * retrocedan un espacio en el array para no dejar ese hueco en blanco.
     */
    public void remLibro(String autor, String titulo) {
        int pos = posicionLibro(autor, titulo);
        if (pos >= 0) {
            for (int i = pos; i < numLibros - 1; i++) {
                libros[i] = libros [i + 1];
            }
            libros[numLibros - 1] = null;
            numLibros--;
        }
    }

    /**
     * Método getter que nos dice el precio de un libro dados un autor y el título del libro.
     * @return Nos devuelve el precio. En caso de que no encuentre el libro nos devolverá cero.
     */
    public double getPrecioBase(String autor, String titulo) {
        int pos = posicionLibro(autor, titulo);
        return (pos < 0) ? 0 : libros[pos].getPrecioBase();
    }

    /**
     * Método getter que nos dice el precio final de un libro (IVA incluido) dados un autor y el título del libro.
     * @return Nos devuelve el precio final. En caso de que no encuentre el libro nos devolverá cero.
     */
    public double getPrecioFinal(String autor, String titulo) {
        int pos = posicionLibro(autor, titulo);
        return (pos < 0) ? 0 : libros[pos].getPrecioFinal(); // Si pos < 0 entonces, devuelve 0, sino devuelve el precioFinal.
    }

    /**
     * Método getter que nos dice el autor, el título y el precio final de un libro (IVA incluido) dados un autor y
     * el título del libro. Método que creamos extra para ver mejor la información en la clase MainLibreria.
     * @return Nos devuelve el precio final. En caso de que no encuentre el libro nos devolverá cero.
     */
    public String getInformaciónPrecioFinal(String autor, String titulo) {
        return "Autor: " + autor + "|| Título: " + titulo + "|| Precio final: " + getPrecioFinal(autor, titulo);
    }

    /**
     * Método toString que nos muestra todas las posiciones del array libros.
     * @return Devuelve el array libros en el formato establecido [Libro1,Libro2, Libro2, LibroN].
     */
    @Override
    public String toString() {
        String salida = "/n[";
        for (int i = 0; i < numLibros; i++) {
            salida += libros [i];
            if (i < numLibros - 1) {
                salida+=",";
            }
        }
        salida += ("]");
        return salida;
    }
}
