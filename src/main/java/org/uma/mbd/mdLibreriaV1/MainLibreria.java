package org.uma.mbd.mdLibreriaV1;

import org.uma.mbd.mdLibreriaV1.libreria.Libreria;

public class MainLibreria {
    public static void main(String [] args) {

        Libreria libreria = new Libreria(3);

        // Añadimos libros al array libros
        libreria.addLibro("george orwell", "1984", 8.20);
        libreria.addLibro("Philip K. Dick", "¿Sueñan los androides con ovejas eléctricas?", 3.50);
        libreria.addLibro("Isaac Asimov", "Fundación e Imperio", 9.40);
        libreria.addLibro("Ray Bradbury","Fahrenheit 451",7.40);
        libreria.addLibro("Alex Huxley", "Un Mundo Feliz",6.50);
        libreria.addLibro("Isaac Asimov", "La Fundación",7.30);
        libreria.addLibro("William Gibson", "Neuromante", 8.30);
        libreria.addLibro("Isaac Asimov","Segunda Fundación",8.10);
        libreria.addLibro("Isaac Newton", "arithmetica universalis", 7.50);
        libreria.addLibro("George Orwell", "1984", 6.20);
        libreria.addLibro("Isaac Newton", "Arithmetica Universalis", 10.50); // Como el libro ya se agregó anteriormente se sustituye

        // Imprimimos nuestro array libros en su estado inicial
        System.out.println(libreria);

        // Eliminamos algunos libros del array libros
        libreria.remLibro("George Orwell", "1984");
        libreria.remLibro("Alex Huxley", "Un Mundo Feliz");
        libreria.remLibro("Isaac Newton", "Arithmetica Universalis");

        // Imprimimos nuestro array libros después de haber eliminado algunos libros
        System.out.println(libreria);

        // Los libros que anteriormente hemos eliminado deben de salir ahora con precio cero
        System.out.println(libreria.getInformaciónPrecioFinal("George Orwell", "1984"));
        System.out.println(libreria.getInformaciónPrecioFinal("Philip K. Dick", "¿Sueñan los androides con ovejas eléctricas?"));
        System.out.println(libreria.getInformaciónPrecioFinal("isaac asimov", "fundación e imperio"));
        System.out.println(libreria.getInformaciónPrecioFinal("Ray Bradbury", "Fahrenheit 451"));
        System.out.println(libreria.getInformaciónPrecioFinal("Alex Huxley", "Un Mundo Feliz"));
        System.out.println(libreria.getInformaciónPrecioFinal("Isaac Asimov", "La Fundación"));
        System.out.println(libreria.getInformaciónPrecioFinal("william gibson", "neuromante"));
        System.out.println(libreria.getInformaciónPrecioFinal("Isaac Asimov", "Segunda Fundación"));
        System.out.println(libreria.getInformaciónPrecioFinal("Isaac Newton", "Arithmetica Universalis"));
    }
}
