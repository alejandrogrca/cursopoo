package org.uma.mbd.mdAlturasInterfazComparable.alturas;

public interface Seleccion {
    boolean test(Pais pais);
}
