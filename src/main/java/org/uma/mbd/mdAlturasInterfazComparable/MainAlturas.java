package org.uma.mbd.mdAlturasInterfazComparable;

import org.uma.mbd.mdAlturasInterfazComparable.alturas.Pais;
import org.uma.mbd.mdAlturasInterfazComparable.alturas.Paises;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class MainAlturas {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");
        System.out.println("Menores que 1.7");

        // Cambio con respecto a V1
            Predicate<Pais> pred = p -> p.getAltura() < 1.7;
            List<Pais> lista = paises.selecciona(pred);
            Collections.sort(lista);

        for (Pais pais : paises.selecciona(p -> p.getAltura() < 1.7)) {
            System.out.println(pais);
        }
        System.out.println("En europa");
        for (Pais pais : paises.selecciona(p -> p.getContinente().equals("Europe"))) {
            System.out.println(pais);
        }
    }
}
