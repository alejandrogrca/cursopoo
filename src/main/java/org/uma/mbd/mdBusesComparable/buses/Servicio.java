/*package org.uma.mbd.mdBusesComparable.buses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Predicate;

public class Servicio {

    // Variables de clase
    private String ciudad;
    private Bus[] buses;
    private int TAM = 10;

    public Servicio(String ci) {
        ciudad = ci;
        buses = new Bus[TAM];
    }

    public String getCiudad() {
        return ciudad;
    }

    public Bus[] getBuses() {
        return buses;
    }

    public void leeBuses(String file) throws FileNotFoundException {
        String fichero = "recursos/mdBus/buses.txt";
        // Otra forma de acceder al fichero sería:
        // String fichero = "C:\\Users\\Usuario\\IdeaProjects\\CursoPOO\\recursos\\mdBus";
        try(Scanner sc = new Scanner (new File(fichero))) {
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());
            }
        }
    }

    private void leeBuses(Scanner sc) {
        int pos = 0;
        while(sc.hasNextLine()) {
            String linea = sc.nextLine();
            try {
                Bus bus = stringToBus(linea);
                if (pos == buses.length) {
                    buses = Arrays.copyOf(buses, buses.length * 2);
                }
                buses[pos] = bus;
                pos++;
            } catch (InputMismatchException e) {
                System.err.println("ERROR: dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("ERROR: faltan datos en " + linea);
            }
        }
        buses = Arrays.copyOf(buses, pos);
    }

    private Bus stringToBus(String linea) {
        try(Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[:+]");
            int cb = sc.nextInt();
            String mat = sc.next();
            int cl = sc.nextInt();
            Bus bus = new Bus(cb, mat);
            bus.setCodLinea(cb);
            return bus;
        }
    }

    public List<Bus> filtra(Comparator<Bus> comp, Predicate ctr) {
        List<Bus> sol = new ArrayList<>();

        for (Bus bus : buses) {
            if(ctr.test(bus)) {
            sol.add(bus);
            }
        }
        sol.sort(comp);
        return sol;
    }

    public void guarda(String file, Comparator<Bus> comp, Predicate<Bus> ctr) throws FileNotFoundException { // Este método guarda, nos crea un fichero.
        try (PrintWriter pw = new PrintWriter(file)) {
            guarda(pw, ctr);
        }
    }

    public void guarda(PrintWriter pw,Predicate<Bus> ctr) { // Este método guarda, nos imprmime por pantalla
        List<Bus> cumplen = filtra(ctr);
        Collections.sort(cumplen); // También puedo poner: cumplen.sort(null);
        pw.println(ctr);
        for(Bus bus : cumplen) {
            pw.println(bus);
        }
    }
}*/
