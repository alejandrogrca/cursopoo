package org.uma.mbd.mdBusesComparable.buses;

// Implementamos la interfaz Comparable
public class Bus implements Comparable<Bus> { // Cambio con respecto a V1

    // Variables de clase
    private String matricula;
    private int codBus;
    private int codLinea;

    public Bus(int cb, String mat) {
        codBus = cb;
        matricula = mat;
    }

    public void setCodLinea(int codLinea) {
        codLinea = codLinea;
    }

    public int getCodBus() {
        return codBus;
    }

    public String getMatricula() {
        return matricula;
    }

    public int getCodLinea() {
        return codLinea;
    }

    @Override
    public boolean equals (Object obj) {
        boolean res = obj instanceof Bus;
        Bus bus = res ? (Bus) obj : null;
        return res && codBus == bus.codBus && matricula.equalsIgnoreCase(bus.matricula);
    }

    @Override
    public int hashCode() {
        return matricula.toUpperCase().hashCode() + Integer.hashCode(codBus);
    }

    // Cambio con respecto a V1: Añadimos el método compareTo
    @Override
    public int compareTo(Bus bus) {
        return matricula.compareTo(bus.matricula);
    }

    @Override
    public String toString() {
        return "Bus (" + codBus + "," + matricula + "," + codLinea + ")";
    }
}
