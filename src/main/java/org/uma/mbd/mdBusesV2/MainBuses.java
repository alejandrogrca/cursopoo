package org.uma.mbd.mdBusesV2;

import org.uma.mbd.mdBusesV2.buses.Bus;
import org.uma.mbd.mdBusesV2.buses.Servicio;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.function.Predicate;

public class MainBuses {
    public static void main(String [] args) {
        Servicio servicio = new Servicio("Malaga");
        try {
            long ini = System.nanoTime();
//        	System.out.println(servicio.getCiudad());
            servicio.leeBuses("recursos/mdBus/buses.txt");

            PrintWriter pw = new PrintWriter(System.out,true);
            Predicate<Bus> cr1 = b -> b.getCodLinea() == 21;
            Comparator<Bus> ocLinea =
                    (b1, b2) -> Integer.compare(b1.getCodLinea(), b2.getCodLinea());
            Comparator<Bus> comp = ocLinea.thenComparing(Comparator.naturalOrder());
            servicio.guarda("recursos/mdBus/linea21.txt", comp, cr1);
            servicio.guarda(pw, comp, cr1);

            //Criterio cr2 = new EnMatricula("29");
            //servicio.guarda("recursos/mdBus/contiene29.txt", cr2);
            Predicate<Bus> ctr2 = bus -> bus.getMatricula().contains("29");
            servicio.guarda(pw, comp, ctr2);


            Predicate<Bus> cr3 = bus -> bus.getMatricula().startsWith("2");
            servicio.guarda(pw, comp, cr3);
            System.out.println((System.nanoTime() - ini)/1_000_000);

        } catch (FileNotFoundException e) {
            System.err.println("No existe el fichero de entrada o de salida");
        }
    }
}
