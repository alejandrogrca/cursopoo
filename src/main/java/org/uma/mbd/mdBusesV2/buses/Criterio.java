package org.uma.mbd.mdBusesV2.buses;

public interface Criterio {
    boolean esSeleccionable(Bus bus);
}
