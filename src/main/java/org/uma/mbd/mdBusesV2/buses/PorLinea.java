package org.uma.mbd.mdBusesV2.buses;

public class PorLinea implements Criterio {

    private int codLinea;

    public PorLinea(int cl) {
        codLinea = cl;
    }

    @Override
    public boolean esSeleccionable(Bus bus) {
        return bus.getCodLinea() == codLinea;
    }

    @Override
    public String toString() {
        return "Por línea " + codLinea;
    }


}
