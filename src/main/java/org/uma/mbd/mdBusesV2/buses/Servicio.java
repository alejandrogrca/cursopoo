package org.uma.mbd.mdBusesV2.buses;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.io.File;
import java.util.function.Predicate;

public class Servicio {
    private String ciudad;
    private List<Bus> buses;

    public Servicio(String ci) {
        ciudad = ci;
        buses = new LinkedList<>();
    }

    public String getCiudad() {
        return ciudad;
    }

    public List<Bus> getBuses() {
        return  buses;
    }

    public void leeBuses(String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner(new File(file))) {
            leeBuses(sc);
        }
    }

    private void leeBuses(Scanner sc) {
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
            try {
                Bus bus = stringToBus(linea);
                buses.add(bus);
            } catch (InputMismatchException e) {
                System.err.println("ERROR dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("ERROR faltan datos en " + linea);
            }
        }
    }

    private Bus stringToBus(String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[,]");
            int cb = sc.nextInt();
            String mat = sc.next();
            int cl = sc.nextInt();
            Bus bus = new Bus(cb, mat);
            bus.setCodLinea(cl);
            return bus;
        }
    }

    public List<Bus> filtra(Comparator<Bus> comp, Predicate<Bus> ctr ) {
        Set<Bus> sol = new TreeSet<>(comp);
        for (Bus bus : buses) {
            if (ctr.test(bus)) {
                sol.add(bus);
            }
        }
        return new ArrayList<>(sol);
    }

    public void guarda(String file, Comparator<Bus> comp, Predicate<Bus> ctr) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(file)) {
            guarda(pw, comp, ctr);
        }
    }

    public void guarda(PrintWriter pw, Comparator<Bus> comp, Predicate<Bus> ctr) {
        List<Bus> cumplen = filtra(comp, ctr);
        for(Bus bus : cumplen) {
            pw.println(bus);
        }
    }
}
