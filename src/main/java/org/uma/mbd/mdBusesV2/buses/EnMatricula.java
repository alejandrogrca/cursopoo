package org.uma.mbd.mdBusesV2.buses;

public class EnMatricula implements Criterio {

    // Variable de clase
    private String trozoMatricula;

    public EnMatricula(String trozo) {
        this.trozoMatricula = trozo;
    }

    @Override
    public boolean esSeleccionable(Bus bus) {
        return bus.getMatricula().indexOf(trozoMatricula) >= 0; //index of no es booleano por eso hay que poner mayor o igual.
        // return bus.getMatricula().contains(trozoMatricula); otra forma de hacerlo. contains of es ya una expresión booleana.
    }

    @Override
    public String toString() {
        return "En matricula " + trozoMatricula;
    }

}
