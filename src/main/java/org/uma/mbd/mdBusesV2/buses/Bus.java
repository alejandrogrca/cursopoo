package org.uma.mbd.mdBusesV2.buses;

public class Bus implements Comparable<Bus> {
    private int codBus;
    private int codLinea;
    private String matricula;

    public Bus(int cb, String mat) {
        codBus = cb;
        matricula = mat;
    }

    public int getCodBus() {
        return codBus;
    }

    public int getCodLinea() {
        return codLinea;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setCodLinea(int codLinea) {
        this.codLinea = codLinea;
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = obj instanceof Bus;
        Bus bus = res ? (Bus) obj : null;
        return res && codBus == bus.codBus
                && matricula.equalsIgnoreCase(bus.matricula);
    }

    @Override
    public int hashCode() {
        return matricula.toUpperCase().hashCode() + Integer.hashCode(codBus);
    }

    @Override
    public int compareTo(Bus bus) {
        return matricula.compareTo(bus.matricula);
    }

    @Override
    public String toString() {
        return "Bus(" + codBus + ", " + matricula + ", " + codLinea + ")";
    }

}
