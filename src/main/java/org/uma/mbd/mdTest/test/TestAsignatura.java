package org.uma.mbd.mdTest.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Clase TestAsignatura
 * @author AlejandroGrca
 */
/*public class TestAsignatura extends Test {

    // Variables de clase
    private String nombre;
    private static final double APROBADO = 5;
    private double valorAciertos;
    private double valorErrores;
    private Test[] examenes;

    public TestAsignatura(String nombre, double valorAciertos, double valorErrores, String[] datos) {
        this.valorAciertos = valorAciertos;
        this.valorErrores = valorErrores;
        this.nombre = nombre;
        extraeDatos(datos);
    }

    public String getNombre() {
        return nombre;
    }

    public void extraeDatos(String datos[]) {
        examenes = new Test[datos.length];
        for (int i = 0; i < datos.length; i++) {
            try (Scanner sc = new Scanner(datos[i])) {
                sc.useDelimiter("[:+]"); // El delimitador puede ser dos puntos o también un más
                String nombreAlumno = sc.next();
                int ac = sc.nextInt();
                int er = sc.nextInt();
                Test test = new Test(nombreAlumno, ac, er);
                examenes[i] = test; // Guardo la información en mi array examenes
            }
        }
    }

    public TestAsignatura(String nombre, String[] examenes) {
        this(nombre, 1, 0, examenes);
    }

    public double getNotaMedia() {
        double sum = 0;
        for (Test test : examenes) {
            sum += test.getCalificacion(valorAciertos, valorErrores);
        }
        return sum / examenes.length;
    }

    public int getAprobados() {
        int na = 0;
        for (Test test : examenes) {
            if (test.getCalificacion(valorAciertos, valorErrores) >= APROBADO) {
                na++;
            }
        }
        return na;
    }

    @Override
    public Test[] getTestAprobados() {
        Test[] testAp = new Test[examenes.length];
        int pos = 0;
        for (Test test : examenes) {
            if (test.getCalificacion(valorAciertos, valorErrores) <= APROBADO) {
                testAp[pos] = test;
                pos++;
            }
        }
        Arrays.copyOf(testAp, pos); // Modificamos el array para no dar un array de tamaño examenes cuando tenga menos posiciones (número aprobados)
    }

    public TestAsignatura(String n, double va, double vf) { // n nombre asignatura
        nombre = n;
        valorAciertos = va;
        valorErrores = vf;
    }

    public leeDatos(String file) throws FileNotFoundException {
        try(Scanner sc = new Scanner (new File(file))) {
            while (sc.hasNextLine()) {
                System.out.println(sc.nextLine());
            }
        }
    }

    public void leeDatos(Scanner sc) {
        examenes = new Test[5]; // el tamaño no lo sabemos, lo ponemos por ejemplo a cinco
        int pos = 0;
        while(sc.hasNextLine()) {
            String linea = sc.nextLine();
            Test test = stringToTest(linea);
            if(pos == examenes.length) {
                examenes = Arrays.copyOf(examenes, examenes.length*2);
            }
            examenes[pos] = test;
            pos++;
        }
        examenes = Arrays.copyOf(examenes, pos);
    }

      private Test stringToTest(String linea) { // coge una línea y la convierte en un test
                try (Scanner sc = new Scanner(linea)) {
                    sc.useDelimiter("[:+]");
                    String nombreAl = sc.next();
                    int ac = sc.nextInt();
                    int er = sc.nextInt();
                    Test test = new Test(nombreAl, ac, er);
                    return test;
                }
    }
}*/





