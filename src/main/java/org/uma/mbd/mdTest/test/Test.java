package org.uma.mbd.mdTest.test;

/**
 * Clase Test
 * @author AlejandroGrca
 */
public class Test {

    // Variables de clase
    private String alumno;
    private int aciertos;
    private int errores;

    /**
     * Método constructor que establece el nombre del alumno, el número de errores y el número de aciertos
     * @param alumno
     * @param aciertos
     * @param errores
     */
    public Test(String alumno, int aciertos, int errores) {
        this.alumno = alumno;
        this.aciertos  = aciertos;
        this.errores = errores;
    }

    public String getAlumno() {
        return alumno;
    }

    public int getAciertos() {
        return aciertos;
    }

    public int getErrores() {
        return errores;
    }

    @Override
    public boolean equals(Object c) {
        boolean res = c instanceof Test;
        Test p = res ? (Test)c : null;
        return res && alumno.equalsIgnoreCase(p.alumno);
    }

    @Override
    public int hashCode() {
        return alumno.toLowerCase().hashCode();
    }

    public double getCalificacion(double valAc, double valErr) {
        if (valAc <= 0 || valErr > 0) {
            throw new IllegalArgumentException("Valores de entrada erroneos");
        }
        double resultado = valAc * aciertos + valErr * errores;
        return resultado;
    }

    @Override
    public String toString() {
        return alumno.toUpperCase() + " [" + aciertos + "," + errores + "]";
    }
}
