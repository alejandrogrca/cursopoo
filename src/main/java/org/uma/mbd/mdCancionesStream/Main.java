package org.uma.mbd.mdCancionesStream;

import org.uma.mbd.mdCancionesStream.canciones.Cancion;
import org.uma.mbd.mdCancionesStream.canciones.Genero;
import org.uma.mbd.mdCancionesStream.canciones.MiColector;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static List<Cancion> listaCanciones;

	public static void main(String[] args) throws IOException {
		listaCanciones = Cancion.creaLista("recursos/mdCancionesStream/fiesta.txt");

		// Listado de canciones
		// listaCanciones.forEach(System.out::println);
		// System.out.println("Duraciones");
		// getDuraciones().forEach(System.out::println);
		// System.out.println("Canciones de los años 1980, 1984, 1992 y 1993");
		// int [] anos = {1980, 1984, 1992, 1993};
		// cancionesPorFechaDada(anos).forEach(System.out::println);
		// System.out.println("Canciones del genero DANCE");
		// cancionesDeGenero(Genero.DANCE).forEach(System.out::println);
		// System.out.println("Muestra canciones");
		// muestraCanciones().forEach(System.out::println);
		// System.out.println("Incrementa puntuación");
		// incrementaPuntuacionTodasCanciones();
		// muestraCanciones().forEach(System.out::println);
		// System.out.println("Son de ROCK todas las de los 80");
		// System.out.println(esROCKDeLos80());
		// System.out.println("Primera duración menor de 300sg");
		// System.out.println(primeraDuracionDeMenosDe(300));
		// System.out.println("Canción HIPHOP mas larga");
		// System.out.println(cancionMasLargaDelGenero(Genero.HIPHOP));
		// System.out.println("Arrays de los interprestes de ROCK");
		// System.out.println(Arrays.toString(interpretes(Genero.ROCK)));
		// System.out.println("Conjunto ordenado de interpretes de ROCK");
		// System.out.println(interpretesOrd(Genero.ROCK));
		// System.out.println(interpretesOrd2(Genero.ROCK));
		// System.out.println("Lista de interpretes de ROCK");
		// System.out.println(interpretesList(Genero.ROCK));
		// System.out.println("Conjunto ordenado de interpretes de ROCK, 2");
		// System.out.println(setInterpretesOrdenado(Genero.ROCK));
		// System.out.println("Conjunto de autores de ROCK, 2");
		// System.out.println(interpretesDeGenero(Genero.ROCK));
		// System.out.println("Suma longitudes de los títulos");
		// System.out.println(sumaLongitudesTitulos());
		// System.out.println("Map de menores, mayores de 150sg");
		// System.out.println(cancionesMenoresMayoresQue(Duration.ofSeconds(150)));
		// System.out.println("Map canciones por intérprete");
		// System.out.println(cancionesPorInterprete());
		// System.out.println("Duraciones Set por genero");
		// System.out.println(duracionesSetPorGenero());
		// System.out.println("Numero de canciones por genero");
		// System.out.println(numeroCancionesPorGenero());
		// System.out.println("Número de canciones por duración");
		// System.out.println(numeroCancionesPorDuracion());
		// System.out.println("Duracion canciones del genero POP");
		// System.out.println(duracionCancionesDelGenero(Genero.POP));
		// System.out.println("Duracion canciones del genero POP 2");
		// System.out.println(duracionCancionesDelGenero2(Genero.POP));
		// System.out.println("Duracion canciones del genero POP 3");
		// System.out.println(duracionCancionesDelGenero3(Genero.POP));
		// System.out.println("String de intérpretes");
		// System.out.println(stringInterpretes());

		//System.out.println(sior());
		//System.out.println(sior2());
		System.out.println(icc());
		// System.out.println("Numero de canciones por genero");
		// System.out.println(numeroDeCancionesPorGenero());
		// System.out.println("Numero de canciones por genero 2");
		// System.out.println(numeroDeCancionesPorGenero2());
		// System.out.println("Numero de canciones por genero 3");
		// System.out.println(numeroDeCancionesPorGenero3());
		// System.out.println(numeroCancionesPorGeneroYAño());
		// System.out.println(listaCancionesPorGeneroYAño());
	}

	// stream con todas las duraciones de las canciones de la lista
	public static Stream<Duration> getDuraciones() {
		return listaCanciones.stream().map(Cancion::getDuracion);
	}

	// stream con todas las canciones de años dados
	public static Stream<Cancion> cancionesPorFechaDada(int[] annos) {
		return Arrays.stream(annos).boxed().flatMap(
				i -> listaCanciones.stream().filter(c -> c.getAño() == i));
	}

	// stream con solo las canciones de un genero dado
	public static Stream<Cancion> cancionesDeGenero(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen);
	}

	// muestra los elementos de un stream y sigue con él
	public static Stream<Cancion> muestraCanciones() {
		return listaCanciones.stream().peek(System.out::println);
	}

	// Incrementa en uno las puntuaciones de todas las canciones
	public static void incrementaPuntuacionTodasCanciones() {
		listaCanciones.stream()
				.forEach(x -> x.setPuntuacion(x.getPuntuacion() + 1));
	}

	// conocer la primera duración que haya menor que los segundos dados y si no
	// hay niguna,
	// devolver una duración de 0 segundos
	public static Duration primeraDuracionDeMenosDe(int seconds) {
		return listaCanciones.stream().map(Cancion::getDuracion)
				.filter(x -> x.compareTo(Duration.ofSeconds(seconds)) < 0)
				.findFirst().orElse(Duration.ofSeconds(0));
	}

	// Obtener la cancion mas larga de un genero dado
	public static Cancion cancionMasLargaDelGenero(Genero g) {
		return listaCanciones.stream().filter(x -> x.getGenero() == g)
				.max(Comparator.comparing(Cancion::getDuracion)).get();
	}

	// devuelve un array con los interpretes de un genero dado
	public static String[] interpretes(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete).toArray(String[]::new);
	}

	/*
	 * public static Set<String> interpretesOrdOld(Genero gen) { Set<String> set
	 * = new TreeSet<>(); for (Cancion cancion : listaCanciones) { if
	 * (cancion.getGenero() == gen) { set.add(cancion.getInterprete()); } }
	 * return set; }
	 */

	// devuelve un conjunto ordenado con los intérpretes que hacen un genero
	// dado
	public static Set<String> interpretesOrd(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete)
				.collect(TreeSet<String>::new, Set::add, Set::addAll);
	}

	// devuelve un conjunto ordenado con los intérpretes que hacen un genero
	// dado
	public static Set<String> interpretesOrd2(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete).collect(new MiColector());
	}

	// devuelve una lista con los autores que hacen un genero
	public static List<String> interpretesList(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete).collect(Collectors.toList());
	}

	// devuelve el conjunto de los interpretes ordenados de un genero dado
	public static Set<String> setInterpretesOrdenado(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete)
				.collect(Collectors.toCollection(TreeSet::new));
	}

	// Conjunto de autores que hacen un genero dado
	public static Set<String> interpretesDeGenero(Genero g) {
		return listaCanciones.stream().filter(x -> x.getGenero() == g).collect(
				Collectors.mapping(Cancion::getInterprete, Collectors.toSet()));

	}

	// Devuelve la suma de las longitudes de los títulos de la canciones
	public static Integer sumaLongitudesTitulos() {
		return listaCanciones.stream()
				.collect(Collectors.summingInt(c -> c.getTitulo().length()));
	}

	// Crea una correspondencia con las canciones cuya duración es menor
	// que una dada y los que son mayores
	public static Map<Boolean, List<Cancion>> cancionesMenoresMayoresQue(
			Duration d) {
		return listaCanciones.stream().collect(Collectors
				.partitioningBy(c -> c.getDuracion().compareTo(d) <= 0));
	}

	// Crea un diccionario con una lista de canciones por intérprete
	public static Map<String, List<Cancion>> cancionesPorInterprete() {
		return listaCanciones.stream()
				.collect(Collectors.groupingBy(Cancion::getInterprete));
	}

	// Crea un diccionario con el genero como claves y
	// un conjunto de duraciones como valor
	public static Map<Genero, Set<Duration>> duracionesSetPorGenero() {
		return listaCanciones.stream().collect(Collectors.groupingBy(
				Cancion::getGenero,
				Collectors.mapping(Cancion::getDuracion, Collectors.toSet())));
	}

	// Crea un diccionario con el genero como claves y
	// el número de canciones de ese género como valor
	public static Map<Genero, Long> numeroCancionesPorGenero() {
		return listaCanciones.stream().collect(Collectors
				.groupingBy(Cancion::getGenero, Collectors.counting()));
	}

	// Crea un diccionario ordenado con el
	// número de canciones por duración
	public static Map<Duration, Long> numeroCancionesPorDuracion() {
		return listaCanciones.stream().collect(Collectors.groupingBy(
				Cancion::getDuracion, TreeMap::new, Collectors.counting()));
	}

	// Crea un opcional con la duración de todas las canciones de un genero dado
	public static Optional<Duration> duracionCancionesDelGenero(Genero g) {
		return listaCanciones.stream().filter(x -> x.getGenero() == g)
				.map(Cancion::getDuracion).reduce(Duration::plus);
	}

	// Crea un diccionario que cuenta cuanto duran todas
	// las canciones que hay en un genero
	public static Duration duracionCancionesDelGenero2(Genero g) {
		return listaCanciones.stream().filter(x -> x.getGenero() == g)
				.map(Cancion::getDuracion)
				.reduce(Duration.ofSeconds(0), Duration::plus);
	}

	// Suma de las duraciones de las canciones de un genero (con suma)
	public static Duration duracionCancionesDelGenero3(Genero g) {
		return listaCanciones.stream().filter(x -> x.getGenero() == g).reduce(
				Duration.ofSeconds(0), (x, y) -> x.plus(y.getDuracion()),
				Duration::plus);
	}

	public static Set<String> sior() {
		Comparator<String> ci = Comparator.<String>naturalOrder().reversed();
		return listaCanciones.stream().map(Cancion::getInterprete)
				.collect(Collectors.toCollection(() -> new TreeSet<>(ci)));
	}

	public static Set<String> sior2() {
		Comparator<String> ci = Comparator.<String>naturalOrder().reversed();
		return listaCanciones.stream()
				.collect(Collectors.mapping(Cancion::getInterprete,
						Collectors.toCollection(() -> new TreeSet<>(ci))));
	}

	public static String stringInterpretes() {
		return listaCanciones.stream().map(Cancion::getInterprete)
				.collect(Collectors.joining(" - ", " < ", " > "));
	}

	////// OTRAS
	// Muestra las duraciones de las canciones de un genero
	public static void duraciones(Genero g) {
		listaCanciones.stream().filter(x -> x.getGenero() == g)
				.map(Cancion::getDuracion).forEach(System.out::println);
	}

	// Devuelve un conjunto con los interpretes que hacen un genero
	public static Set<String> interpretesSet(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete).collect(Collectors.toSet());
	}

	// Devuelve un map con los interpretes que hacen un genero y su longitud
	public static Map<String, Integer> interpretesMap(Genero gen) {
		return listaCanciones.stream().filter(x -> x.getGenero() == gen)
				.map(Cancion::getInterprete)
				.collect(Collectors.toMap(x -> x, String::length));
	}

	// Para cada interprete el conjunto de las canciones
	public static Map<String, Set<Cancion>> icc() {
		return listaCanciones.stream()
				.collect(Collectors.groupingBy(Cancion::getInterprete,
						Collectors.toSet()));
	}

	// Para cada interprete el conjunto de las canciones
	public static Map<String, Set<String>> ict() {
		return listaCanciones.stream()
				.collect(Collectors.groupingBy(
						Cancion::getInterprete,
						Collectors.mapping(Cancion::getTitulo,
								Collectors.toSet())));
	}

	// Numero de canciones por genero
	public static Map<Genero, Long> numeroDeCancionesPorGenero() {
		return listaCanciones.stream().map(Cancion::getGenero)
				.collect(Collectors.groupingBy(Function.identity(),
						Collectors.counting()));
	}

	// Numero de canciones por genero 2
	public static Map<Genero, Integer> numeroDeCancionesPorGenero2() {
		return listaCanciones.stream().map(Cancion::getGenero)
				.collect(Collectors.groupingBy(Function.identity(),
						Collectors.summingInt(x -> 1)));
	}

	// Numero de canciones por genero 3
	public static Map<Genero, Integer> numeroDeCancionesPorGenero3() {
		return listaCanciones.stream().collect(Collectors
				.groupingBy(Cancion::getGenero, Collectors.summingInt(x -> 1)));
	}

	public boolean esROCKDelos80() {
		return listaCanciones.stream().filter(x -> x.getGenero() == Genero.ROCK)
				.map(Cancion::getAño).allMatch(x -> x < 1990 && x >= 1980);
	}

	public static Map<Genero, Map<Integer, Long>> numeroCancionesPorGeneroYAño() {
		return listaCanciones.stream().collect(Collectors.groupingBy(
				Cancion::getGenero,
				Collectors.groupingBy(Cancion::getAño, Collectors.counting())));
	}

	public static Map<Genero, Map<Integer, List<Cancion>>> listaCancionesPorGeneroYAño() {
		return listaCanciones.stream().collect(Collectors.groupingBy(
				Cancion::getGenero, Collectors.groupingBy(Cancion::getAño)));
	}

	public static Map<Genero, Set<Cancion>> cancionesPorGenero() {
		return listaCanciones.stream().collect(Collectors.groupingBy(
				Cancion::getGenero, Collectors.toCollection(TreeSet::new)));
	}

	public static Map<Long, Set<Genero>> gpc() {
		Map<Genero, Long> map = numeroDeCancionesPorGenero();
		return map.entrySet().stream().collect(Collectors.groupingBy(
				Map.Entry::getValue, TreeMap::new,
				Collectors.mapping(Map.Entry::getKey, Collectors.toSet())));
	}

	public static Map<Genero, Long> ndcpg() {
		return listaCanciones.stream().map(Cancion::getGenero)
				.collect(Collectors.groupingBy(Function.identity(),
						Collectors.counting()));
	}

	public static Map<Genero, Map<Integer, Long>> ngpa() {
		return listaCanciones.stream().collect(Collectors.groupingBy(
				Cancion::getGenero,
				Collectors.groupingBy(Cancion::getAño, Collectors.counting())));
	}

	public static Map<Genero, Map<Integer, List<Cancion>>> mdm() {
		return listaCanciones.stream()
				.collect(Collectors.groupingBy(Cancion::getGenero,
						Collectors.groupingBy(Cancion::getAño)));
	}

	public static Map<Genero, Map<Integer, Set<String>>> mdmsi() {
		return listaCanciones.stream()
				.collect(Collectors.groupingBy(Cancion::getGenero,
						Collectors.groupingBy(Cancion::getAño,
								Collectors.mapping(Cancion::getInterprete,
										Collectors.toSet()))));
	}
}









