package org.uma.mbd.mdCancionesStream.canciones;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Cancion implements Comparable<Cancion> {
    private String interprete;
    private String titulo;
    private Duration duracion;
    private Genero genero;
    private double puntuacion;
    private int año;

    public Cancion(String t, String i, int m, int s, Genero g, double r, int a) {
        titulo = t;
        interprete = i;
        genero = g;
        duracion = Duration.ofSeconds(m * 60 + s);
        puntuacion = r;
        año = a;
    }

    public String getInterprete() {
        return interprete;
    }

    public String getTitulo() {
        return titulo;
    }

    public Duration getDuracion() {
        return duracion;
    }

    public Genero getGenero() {
        return genero;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double r) {
        puntuacion = r;
    }

    public int getAño() {
        return año;
    }

    @Override
    public boolean equals(Object o) {
        boolean res = o instanceof Cancion;
        Cancion c = res ? (Cancion) o : null;
        return res &&
                titulo.equalsIgnoreCase(c.titulo) &&
                interprete.equalsIgnoreCase(c.interprete) &&
                duracion.equals(c.duracion);
    }

    @Override
    public int hashCode() {
        return titulo.toUpperCase().hashCode() +
                interprete.toUpperCase().hashCode() +
                duracion.hashCode();
    }

    @Override
    public int compareTo(Cancion c) {
        int res = titulo.compareToIgnoreCase(c.titulo);
        if (res == 0) {
            res = interprete.compareToIgnoreCase(c.interprete);
        }
        if (res == 0) {
            res = duracion.compareTo(c.duracion);
        }
        return res;
    }

    @Override
    public String toString() {
        String salida = "(" +duracion + ", " + puntuacion + ", ";
        salida += titulo.toUpperCase() + ", ";
        salida += interprete + ", " + genero + ", " + año + ")";
        return salida;
    }

    public static List<Cancion> creaLista(String fichero) throws IOException {
        List<Cancion> listaCanciones;
        try (Stream<String> lineas = Files.lines(Paths.get(fichero))) {
            listaCanciones = lineas.map(Cancion::stringToCancion)
                    .collect(Collectors.toList());
        }
        return listaCanciones;
    }

    private static Cancion stringToCancion(String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[,]");
            sc.useLocale(Locale.ENGLISH);
            String tit = sc.next();
            String in = sc.next();
            int m = sc.nextInt();
            int s = sc.nextInt();
            Genero g = Genero.valueOf(sc.next());
            double d = sc.nextDouble();
            int a = sc.nextInt();
            Cancion c = new Cancion(tit, in, m, s, g, d, a);
            return c;
        } catch (InputMismatchException e) {
            throw new RuntimeException(
                    "Minutos o segundos o puntos no numéricos : " + linea);
        } catch (NoSuchElementException e) {
            throw new RuntimeException("Faltan datos : " + linea);
        }
    }
}
