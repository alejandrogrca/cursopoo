package org.uma.mbd.mdCancionesStream.canciones;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
/**
 * Created by pacog on 5/7/16.
 */
public class MiColector implements Collector<String, Set<String>, TreeSet<String>> {
    @Override
    public Supplier<Set<String>> supplier() {
        return HashSet::new;
    }
    @Override
    public BiConsumer<Set<String>, String> accumulator() {
        return Set::add;
    }
    @Override
    public BinaryOperator<Set<String>> combiner() {
        return (s1,s2) ->{s1.addAll(s2); return s1;};
    }
    @Override
    public 	Function<Set<String>, TreeSet<String>> finisher() {
        return TreeSet::new;
    }
    @Override
    public 	Set<Characteristics> characteristics() {
        return new HashSet<>(
                Arrays.asList(Characteristics.CONCURRENT,
                        Characteristics.UNORDERED));
    }
}

