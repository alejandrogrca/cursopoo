package org.uma.mbd.mdCancionesStream.canciones;

public enum Genero {
	ROCK, POP, HIPHOP, DANCE
}
