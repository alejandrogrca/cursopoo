package org.uma.mbd.mdCoches.coches;

/**
 * Clase Coche Importado
 * @author AlejandroGrca
 */
public class CocheImportado extends Coche {

    //Variables de clase
    private double homologacion;

    /**
     * * Método constructor que establece las características de los objetos de la clase CccheImportado
     * @param nombre Parámetro para establecer el nombre del objeto de la clase CocheImportado
     * @param precio Parámetro para establecer el precio del objeto de la clase CocheImportado
     * @param homologacion Parámetro para establecer el coste de homologación del objeto de la clase CocheImportado
     */
    public CocheImportado (String nombre, double precio, double homologacion) {
        super(nombre, precio);
        this.homologacion = homologacion;
    }

    /**
     * Método getter para obtener el precio del coche (sin IVA) + homologación
     * @return Devuelve el importe total del coche
     */
    @Override // Sobrescribimos el método getPrecioTotal de la clase Coche
    public double getPrecioTotal() {
        return (super.precio + homologacion) * (1+(super.getIVA()/100));
    }

    /**
     * Método toString para devolver información con un formato específico
     * @return Devuelve un String con la información del nombre y el precio (IVA incluido) del coche
     */
    @Override // Sobrescribimos el método toString de la clase Coche
    public String toString() {
        // Las variables nombre y precio deben de ser públicas en la clase Coche
        return "El nombre del coche es " + nombre + ", el precio base es " + precio
                + ", la homologación es " + homologacion + " y el precio con homologación más el IVA es "
                + getPrecioTotal();
    }
}
