package org.uma.mbd.mdCoches.coches;

/**
 * Clase Coche
 * @author AlejandroGrca
 */
public class Coche {

    // Variables de clase
    public String nombre;
    public double precio;
    private static double PIVA=16;

    /**
     * Método constructor que establece las características de los objetos de la clase Ccche
     * @param nombre Parámetro para establecer el nombre del objeto de la clase Coche
     * @param precio Parámetro para establecer el precio del objeto de la clase Coche
     */
    public Coche(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    /**
     * Método setter para cambiar el IVA del coche (por defecto se estableció como 16)
     * @param IVA Valor que tomará el nuevo IVA
     */
    public static void setPiva(double IVA) {
        PIVA = IVA; // La variable de clase PIVA debe de ser estática ya que el método lo es
    }

    /**
     * Método getter para obtener el IVA del coche (por defecto se estableció como 16)
     * @return IVA del coche
     */
    public double getIVA() {
        return PIVA;
    }

    /**
     * Método getter para obtener el precio del coche (IVA incluido)
     * @return Devuelve el importe total del coche
     */
    public double getPrecioTotal() {
        double total = precio + precio*PIVA/100;
        return total;
    }

    /**
     * Método toString para devolver información con un formato específico
     * @return Devuelve un String con la información del nombre y el precio (IVA incluido) del coche
     */
    @Override // Es necesario en el caso de que tengamos otros métodos toString en otras clases
    public String toString() {
        return "El nombre del coche es " + nombre + ", el precio base es " + precio + " y el precio con IVA es "
                + getPrecioTotal();
    }
}
