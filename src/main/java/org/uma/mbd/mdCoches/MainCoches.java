package org.uma.mbd.mdCoches;

import org.uma.mbd.mdCoches.coches.Coche;
import org.uma.mbd.mdCoches.coches.CocheImportado;

public class MainCoches {

    static Coche ArrayCoches[] = {
            new Coche("Seat Panda", 15000),
            new CocheImportado("Ferrari T-R", 65000, 8000),
            new Coche("Seat Toledo", 21000),
            new CocheImportado("Jaguar XK", 41000, 6000),
            new CocheImportado("Porche GT3", 44000, 7000)
    };

    public static void main (String[] args) {

        for(Coche c: ArrayCoches) {
            System.out.println(c);
        }

        Coche.setPiva(18); // Si el setter setPiva no es estático nos daría error
        System.out.println("\nCon IVA de 18%");

        for(Coche c: ArrayCoches) {
            System.out.println(c);
        }
    }
}
