package org.uma.mbd.mdEstadistica.estadistica;

/**
 * Clase Estadistica
 * @author AlejandroGrca
 */
public class Estadistica {

    // Variables de clase
    private double numElementos;
    private double sumaX;
    private double sumaX2;

    /**
     * Método constructor de la clase Estadística, iniciamos todos los parámetros a cero.
     */
    public Estadistica () {
        numElementos=0;
        sumaX=0;
        sumaX2=0;
    }

    /**
     * Método setter para agregar un número a de la clase Estadística.
     * @param d Número que agregamos (este número influye en numElementos, sumaX y sumaX2).
     */
    public void setAgrega(double d) {
        numElementos++;
        sumaX += d;
        sumaX2 += Math.pow(d,2);
    }

    /**
     * Método setter para agregar un número a de la clase Estadística.
     * @param d Número que agregamos (este número influye en numElementos, sumaX y sumaX2).
     * @param n Numero de veces que agregamos el número anterior.
     */
    public void setAgrega(double d, int n) {
        numElementos += n;
        sumaX += d*n;
        sumaX2 += Math.pow(d,2)*n;
    }

    /**
     * Método getter para calcular la media.
     * @return Nos devuelve la media de los números introducidos.
     */
    public double getMedia() {
        return sumaX/numElementos;
    }

    /**
     * Método getter para calcular la varianza.
     * @return Nos devuelve la varianza de los números introducidos.
     */
    public double getVarianza() {
        double media = sumaX / numElementos;
        double varianza = (sumaX2 / numElementos) - Math.pow(media,2);
        return varianza;
    }

    /**
     * Método getter para calcular la desviación típica.
     * @return Nos devuelve la desviación típica de los números introducidos.
     */
    public double getDesviacion() {
        double media = sumaX / numElementos;
        double varianza = (sumaX2 / numElementos) - Math.pow(media,2);
        double desviacion = Math.sqrt(varianza);
        return desviacion;
    }

    /**
     * Método toString para devolver la información con un formato específico.
     * @return Devuelve un String con la información de la media, la varianza y la desviación típica.
     */
    @Override
    public String toString() {
        return "La media es: " + String.format("%.5f",getMedia())
                + "\nLa varianza es: " + String.format("%.5f",getVarianza())
                + "\nLa desviación típica es " + String.format("%.5f",getDesviacion());
    }
}
