package org.uma.mbd.mdEstadistica;

import org.uma.mbd.mdEstadistica.estadistica.Estadistica;

import java.util.Random;

/**
 * Método Main
 * @author AlejandroGrca
 */
public class MainEstadistica {

    public static void main (String[] args) {

        // Introducimos valores de forma manual (usamos los dos métodos setAgrega creados).

        Estadistica estadManual = new Estadistica();

        estadManual.setAgrega(2.0);
        estadManual.setAgrega(3.0);
        estadManual.setAgrega(5.0,2);
        estadManual.setAgrega(6.0,2);
        estadManual.setAgrega(7.0);
        estadManual.setAgrega(2.0);

        System.out.println(estadManual.toString() + "\n");

        // Introducimos valores que se distribuyen según una normal.

        Estadistica estad = new Estadistica();

        Random ran = new Random();

        for(int i = 0 ; i < 1000 ; i++) {
            estad.setAgrega(ran.nextGaussian());
        }

        System.out.println(estad.toString());
    }
}
