package org.uma.mbd.mdAlturasInterfazComparator;

import org.uma.mbd.mdAlturasInterfazComparator.alturas.*;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class MainAlturas {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");
        System.out.println("Menores que 1.7");

        // Bloque 1: Ordenamos por continente
        Predicate<Pais> pred = p -> p.getAltura() < 1.7;
        List<Pais> lista = paises.selecciona(pred);
        Comparator<Pais> comp = new OrdenContinente();
        //Comparator<Pais> comp = new OrdenContinente().reversed(); //Si quisiera ordenar por continente en orden contrario.
        //Comparator<Pais> comp = new OrdenContinente().thenComparing(new OrdenNombre); // Si quisiera ordenar por continente y luego por nombre de países.

        // Bloque 2: Ordenamos por continente, por altura inversa y por nombre inverso.
        Comparator<Pais> oNombre = new OrdenNombre();
        Comparator<Pais> oContinente = new OrdenContinente();
        Comparator<Pais> oAltura = new OrdenAltura();
        Comparator<Pais> comp1 = oContinente.thenComparing(oAltura.reversed()).thenComparing(oNombre.reversed());

        // Bloque 3: Ordenamos por nombre y si son iguales ordenamos por orden natural.
        Predicate<Pais> pred2 = p -> p.getAltura() < 1.7;
        List<Pais> lista2 = paises.selecciona(pred);
        Comparator<Pais> comp2 = new OrdenNombre().thenComparing(Comparator.<Pais>naturalOrder()); // Compara por nombre y en caso de ser iguales igual compara por el orden natural.

        /*Bloque 4: Construimos el comparador sin usar las clases ordenNombre, ordenContinente y ordenAltura. No habría hecho
        * falta crearlas. Usamos lambdas.
        * */
        Comparator<Pais> oNombre1 = (p1, p2) -> p1.getNombre().compareTo(p2.getNombre());
        Comparator<Pais> oContinente1 = (p1, p2) -> p1.getContinente().compareTo(p2.getContinente());
        Comparator<Pais> oAltura1 = (p1, p2) -> Double.compare(p1.getAltura(), p2.getAltura());

        lista.sort(comp);

        for (Pais pais : paises.selecciona(p -> p.getAltura() < 1.7)) {
            System.out.println(pais);
        }

        System.out.println("En europa");

        Predicate<Pais> pred3 = p -> p.getAltura() < 1.7;
        List<Pais> lista3 = paises.selecciona(pred);
        Comparator<Pais> comp3 = new OrdenNombre();
        lista.sort(comp2);
        for (Pais pais : paises.selecciona(p -> p.getContinente().equals("Europe"))) {
            System.out.println(pais);
        }
    }
}
