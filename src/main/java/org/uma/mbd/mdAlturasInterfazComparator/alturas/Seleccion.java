package org.uma.mbd.mdAlturasInterfazComparator.alturas;

public interface Seleccion {
    boolean test(Pais pais);
}
