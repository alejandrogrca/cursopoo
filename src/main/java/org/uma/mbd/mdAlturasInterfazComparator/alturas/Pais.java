package org.uma.mbd.mdAlturasInterfazComparator.alturas;

// Implementamos la interfaz Comparable
public class Pais implements Comparable<Pais> { // Cambio con respecto a V1
    private String nombre;
    private String continente;
    private double altura;

    public Pais(String n, String c, double a) {
        nombre = n;
        continente = c;
        altura = a;
    }

    public String getNombre() {
        return nombre;
    }

    public String getContinente() {
        return continente;
    }

    public double getAltura() {
        return altura;
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = obj instanceof Pais;
        Pais pais = res ? (Pais) obj : null;
        return res && nombre.equals(pais.nombre);
    }

    @Override
    public int hashCode() {
        return nombre.hashCode();
    }

    // Cambio con respecto a V1: Añadimos el método compareTo
    // Ordenamos por altura y si las alturas son iguales por nombre (orden alfabético)
    @Override
    public int compareTo(Pais pais) {
        int res = Double.compare(altura, pais.altura);
        if(res == 0) {
            res = nombre.compareTo(pais.nombre);
        }
        return res;
    }

    @Override
    public String toString() {
        return "Pais(" + nombre + ", " + continente + ", " + altura + ")";
    }
}