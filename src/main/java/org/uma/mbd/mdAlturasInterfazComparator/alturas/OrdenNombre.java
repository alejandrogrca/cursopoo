package org.uma.mbd.mdAlturasInterfazComparator.alturas;

import java.util.Comparator;

public class OrdenNombre implements Comparator<Pais> {

    @Override
    public int compare(Pais p1, Pais p2) {
        return p1.getNombre().compareTo(p2.getNombre());
    }
}
