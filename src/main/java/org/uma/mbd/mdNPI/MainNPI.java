package org.uma.mbd.mdNPI;

import org.uma.mbd.mdNPI.npi.NPI;

/**
 * Método Main
 * @author AlejandroGrca
 */
public class MainNPI {
	public static void main(String [] args) {

		// 5 + (6 - 2) * 3;
		// NPI 5 6 2 - 3 * +
		NPI cpi1 = new NPI();
		cpi1.entra(5);
		cpi1.entra(6);
		cpi1.entra(2);
		cpi1.resta();
		cpi1.entra(3);
		cpi1.multiplica();
		cpi1.suma();
		System.out.println(cpi1.toString());
		System.out.println("Resultado " + cpi1.getResultado());

		// 3 * (6 - 4) + 5;
		// NPI 3 6 4 - * 5 +
		NPI cpi2 = new NPI();
		cpi2.entra(3);
		cpi2.entra(6);
		cpi2.entra(4);
		cpi2.resta();
		cpi2.multiplica();
		cpi2.entra(5);
		cpi2.suma();
		System.out.println(cpi1.toString());
		System.out.println("Resultado " + cpi2.getResultado());
 	}
}
