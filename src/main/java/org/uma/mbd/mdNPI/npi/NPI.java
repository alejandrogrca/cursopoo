package org.uma.mbd.mdNPI.npi;

/**
 * Clase NPI
 * @author AlejandroGrca
 */
public class NPI {

	// Variables de clase.
	private double x,y,z,t;

	/**
	 * Método constructor de la clase NPI, iniciamos todos los parámetros a cero.
	 */
	public NPI() {
		x = y = z = t = 0;
	}

	/**
	 * Método setter para agregar un número.
	 * @param v Número (valor) que asignamos a la variable x.
	 */
	public void entra(double v) {
		t = z;
		z = y;
		y = x;
		x = v;
	}

	/**
	 * Método setter para sumar los valores introducidos anteriormente.
	 */
	public void suma() {
		double res = y + x;
		x = res;
		y = z;
		z = t;
	}

	/**
	 * Método setter para restar los valores introducidos anteriormente.
	 */
	public void resta() {
		double res = y - x;
		x = res;
		y = z;
		z = t;
	}

	/**
	 * Método setter para multiplicar los valores introducidos anteriormente.
	 */
	public void multiplica() {
		double res = y * x;
		x = res;
		y = z;
		z = t;
	}

	/**
	 * Método setter para dividir los valores introducidos anteriormente.
	 */
	public void divide() {
		double res = y / x;
		x = res;
		y = z;
		z = t;
	}

	/**
	 * Método getter para obtener el resultado final.
	 */
	public double getResultado() {
		return x;
	}

	/**
	 * Método toString para devolver la información con un formato específico. La única variablle que puede
	 * tener valor al final del proceso es la x, el resto deben de estar a cero.
	 * @return Devuelve un String con la información de cada una de las variables.
	 */
	@Override
	public String toString() {
		return "NPI(x = "+ x + " y = " + y + " z = " + z + " t = " + t+ ")";
	}
}
