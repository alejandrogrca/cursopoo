package org.uma.mbd.mdKWIC.kwic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class KWIC {

    // Variables de clase
    private Set<String> palNoSig;// Conjunto para seleccionar las palabras mas significativas
    private Map<String, Set<TituloKWIC>> indice;

    public KWIC() {
        palNoSig = new TreeSet<>();
        indice = new TreeMap<>();
    }

    public void palabrasNoSignificativas(String file) throws FileNotFoundException {
        try(Scanner sc = new Scanner(new File(file))) {
            palabrasNoSignificativas(sc);
        }
    }

    public void palabrasNoSignificativas(Scanner sc) {
        while(sc.hasNextLine()) {
            String linea = sc.nextLine();
            try(Scanner scLinea = new Scanner(linea)) {
                scLinea.useDelimiter(" ");
                while(scLinea.hasNext()) {
                    String palabra = scLinea.next().toUpperCase();
                    palNoSig.add(palabra);
                }
            }
        }
    }

    public void generaIndice(String file) throws FileNotFoundException {
        try(Scanner sc = new Scanner(new File(file))) {
            generaIndice(sc);
        }
    }

    public void generaIndice(Scanner sc) {
        while(sc.hasNextLine()) {
            String titulo = sc.nextLine();
            int pos = 0;
            try(Scanner scLinea = new Scanner(titulo)) {
                scLinea.useDelimiter("[ :]+ "); // Delimitadores: espacio y dos puntos. El más se pone por si se repite una o más veces.
                while(scLinea.hasNext()) {
                    String palabra = scLinea.next().toUpperCase();
                    if(!palNoSig.contains(palabra)) { // si el conjunto palNoSig no contiene a la palabra entonnces
                        TituloKWIC tk = new TituloKWIC(titulo, pos);
                        Set<TituloKWIC> set = indice.computeIfAbsent(palabra, k -> new TreeSet<>());
                        set.add(tk);
                    }
                    pos++;
                }
            }
        }
    }

    public void presentaIndice(String file) throws FileNotFoundException {
        try(PrintWriter pw = new PrintWriter(file)) {
            presentaIndice(pw);
        }
    }

    public void presentaIndice(PrintWriter pw) {
        for(String palabra : indice.keySet()) {
            pw.println(palabra);
            for(TituloKWIC tk : indice.get(palabra)) {
                pw.println("\t" + tk);
            }
        }
    }
}
