package org.uma.mbd.mdKWIC.kwic;

public class TituloKWIC implements Comparable<TituloKWIC> {

    // Variables de clase
    private String titulo;
    private int posicion;

    public TituloKWIC(String t, int p) {
        titulo = t;
        posicion = p;
    }

    @Override
    public boolean equals(Object obj) {
        boolean res = obj instanceof TituloKWIC;
        TituloKWIC tk = res ? (TituloKWIC)obj : null;
        return res && posicion == tk.posicion && titulo.equalsIgnoreCase(tk.titulo);
    }

    @Override
    public int hashCode() {
        return titulo.toUpperCase().hashCode() + Integer.hashCode(posicion);
    }

    @Override
    public int compareTo(TituloKWIC tk) {
        int res = Integer.compare(posicion, tk.posicion);
        if(res == 0) {
            res = Integer.compare(posicion, tk.posicion);
        }
        return res;
    }

    @Override
    public String toString() {
        return titulo + "(" + posicion + ")";
    }
}
