package org.uma.mbd.mdIndicePalabrasV1.indices;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public abstract class Indice {
	protected List<String> texto;
	
	public Indice() {
		texto = new ArrayList<String>();
	}
	
	public void agregarLinea(String linea) {
		texto.add(linea);
	}
	
	public abstract void resolver(String delim, Collection<String> noSig);
	
	public abstract void presentarIndiceConsola() ;
}
