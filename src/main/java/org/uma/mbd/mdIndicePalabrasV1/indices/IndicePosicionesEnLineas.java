package org.uma.mbd.mdIndicePalabrasV1.indices;

import java.util.*;

public class IndicePosicionesEnLineas extends Indice {
	private Map<String, Map<Integer, Set<Integer>>> indice;

	public IndicePosicionesEnLineas() {
		super();
		indice = new TreeMap<>();
	}

	public void agregarLinea(String linea) {
		indice.clear();
		super.agregarLinea(linea);
	}

	public void resolver(String delim, Collection<String> noSig) {
		Set<String> palNoSig = new HashSet<>();
		for (String pNS : noSig) {
			palNoSig.add(pNS.toLowerCase());
		}
		int numLinea = 1;
		for (String linea : texto) {
			try (Scanner sc = new Scanner(linea)) {
				sc.useDelimiter(delim);
				int numPal = 1;
				while (sc.hasNext()) {
					String palabra = sc.next().toLowerCase();
					if (!palNoSig.contains(palabra)) {
						Map<Integer, Set<Integer>> map = indice.get(palabra);
						if (map == null) {
							map = new TreeMap<>();
							indice.put(palabra, map);
						}
						// PD indice.get(palabra) == map
						Set<Integer> li = map.get(numLinea);
						if (li == null) {
							li = new TreeSet<>();
							map.put(numLinea, li);
						}
						// PD map.get(numLinea) == li
						li.add(numPal);
					}
					numPal++;
				}
			}
			numLinea++;
		}
	}

	public void presentarIndiceConsola() {
		for (String palabra : indice.keySet()) {
			System.out.println(palabra);
			Map<Integer, Set<Integer>> map = indice.get(palabra);
			for (Integer i : map.keySet()) {
				System.out.print("\t" + i + "\t");
				for (Integer pos : map.get(i)) {
					System.out.print(pos + ".");
				}
				System.out.println();
			}
		}
	}
}
