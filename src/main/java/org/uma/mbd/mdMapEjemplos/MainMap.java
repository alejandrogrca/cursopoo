package org.uma.mbd.mdMapEjemplos;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainMap {

    public static void main (String[] args) {

        Map<String, Integer> map = new HashMap<>(); // Creación de un hashmap vacío.
        map.put("Hola", 4); //Lo vamos llenando y asociamos a la palabra Hola el número 4.
        map.put("Que", 3);
        map.put("Tal", 3);
        map.put("Adiós", 5);

        System.out.println(map); // Visualizamos como se pinta un hashMap

        System.out.println(map.containsKey("Que")); // Preguntamos si está Que dentro del hashMap (ture/false)
        System.out.println(map.containsKey("Que")); // Preguntamos si está 3 dentro del hashMap (ture/false)

        Set<String> set = map.keySet();
        System.out.println(set);

        Collection<Integer> col = map.values();
        System.out.println(col);

        for(String s: map.keySet()) {
            System.out.println(s + " " + map.get(s));
        }
        System.out.println(map.get("Estamos")); // Estamos no está en la correspondiencia pero nos da null en vez de error.
    }
}
