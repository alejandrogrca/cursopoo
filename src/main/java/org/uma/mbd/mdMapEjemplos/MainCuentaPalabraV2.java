package org.uma.mbd.mdMapEjemplos;

import java.util.HashMap;
import java.util.Map;

// Los parámetros los introducimos mediante Run > Edit Configurations

//Programa para contar el número de palabras que hay en un hashMap
public class MainCuentaPalabraV2 {


        public static void main (String[] args) {

            //Programa para contar el número de palabras que hay en un hashMap

            Map<String, Integer> map = new HashMap<>(); // Creo el hashMap.
            for(String palabra : args) { // Leo cada una de las palabras que entran.
                Integer i = map.get(palabra);
                if(i == null) {
                    map.put(palabra,1);
                } else {
                    map.put(palabra, i+1);
                }
            }
            for(String s : map.keySet()) {

                System.out.println(s + " aparece " + map.get(s));
            }

            System.out.println(map);
        }
}
