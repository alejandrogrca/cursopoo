package org.uma.mbd.mdMapEjemplos;

import java.util.HashMap;
import java.util.Map;

// Los parámetros los introducimos mediante Run > Edit Configurations

//Programa para contar el número de palabras que hay en un hashMap
public class MainCuentaPalabraV3 {

    // Los parámetros los introducimos mediante Run > Edit Configurations

    public static void main (String[] args) {

        //Programa para contar el número de palabras que hay en un hashMap

        Map<String, Integer> map = new HashMap<>(); // Creo el hashMap.
        for(String palabra : args) { // Leo cada una de las palabras que entran.
            Integer i = map.getOrDefault(palabra,0);
            if(i == null) {
                map.put(palabra,1);
            } else {
                map.put(palabra, i+1);
            }
        }
        for(String s : map.keySet()) {
            System.out.println(s + " aparece " + map.get(s));
        }
        // Este for hace lo mismo que el anterior pero de forma más eficiente.
        for(Map.Entry<String, Integer> entrada : map.entrySet()) {
            System.out.println(entrada.getKey() + " aparece " + entrada.getValue());
        }
        System.out.println(map);
    }
}
