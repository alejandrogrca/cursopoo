package org.uma.mbd.mdMapEjemplos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Los parámetros los introducimos mediante Run > Edit Configurations

//Programa para saber la posición que ocupa cada una de las de palabras que le pasamos
public class MainCuentaPosicionesV2 {

    public static void main (String[] args) {
        Map<String, List<Integer>> map = new HashMap<>();
        for(int i = 0; i < args.length; i++) {
            String palabra = args[i];

            /* Primera versión
            List<Integer> lista = map.get(palabra);
            if(lista == null) {
                lista = new ArrayList<>();
                map.put(palabra, lista);
            }
            lista.add(i);
            */

            // Segunda versión con lenguaje funcional
            List<Integer> lista = map.computeIfAbsent(palabra, k -> new ArrayList<>());
            lista.add(i);
        }
        for(Map.Entry<String, List<Integer>> entrada : map.entrySet()) {
            System.out.println(entrada.getKey() + " aparece en las posiciones " + entrada.getValue());
        }
    }
}
