package org.uma.mbd.mdMapEjemplos;

import java.util.HashMap;
import java.util.Map;

// Los parámetros los introducimos mediante Run > Edit Configurations

//Programa para contar el número de palabras que hay en un hashMap
public class MainCuentaPalabra {

    public static void main (String[] args) {
        Map<String, Integer> map = new HashMap<>(); // Creo el hashMap.
        for(String palabra : args) { // Leo cada una de las palabras que entran.
            if(map.containsKey(palabra)) {
                map.put(palabra, map.get(palabra) + 1);
            } else {
                map.put(palabra, 1);
            }
        }
        for(String s : map.keySet()) {
            System.out.println(s + " aparece " + map.get(s));
        }
        System.out.println(map);
    }
}
