package org.uma.mbd.mdLibreriaV2;

import org.uma.mbd.mdLibreriaV2.libreria.Libreria;
import org.uma.mbd.mdLibreriaV2.libreria.LibreriaOferta;

public class MainLibreria {
    public static void main(String [] args) {

        // Creamos el string de autores que van a tener descuento.
        String [] autores = {"George Orwell", "Isaac Asimov"};

        // Instanciamos libreria y le establecemos el tamaño y el descuento.
        Libreria libreria = new LibreriaOferta(3, 20, autores);

        // Añadimos libros a la libreria que hemos creado.
        libreria.addLibro("george orwell", "1984", 8.20);
        libreria.addLibro("Philip K. Dick", "¿Sueñan los androides con ovejas eléctricas?", 3.50);
        libreria.addLibro("Isaac Asimov", "Fundación e Imperio", 9.40);
        libreria.addLibro("Ray Bradbury","Fahrenheit 451",7.40);
        libreria.addLibro("Alex Huxley", "Un Mundo Feliz",6.50);
        libreria.addLibro("Isaac Asimov", "La Fundación",7.30);
        libreria.addLibro("William Gibson", "Neuromante", 8.30);
        libreria.addLibro("Isaac Asimov","Segunda Fundación",8.10);
        libreria.addLibro("Isaac Newton", "arithmetica universalis", 7.50);
        libreria.addLibro("George Orwell", "1984", 6.20);
        libreria.addLibro("Isaac Newton", "Arithmetica Universalis", 10.50);

        // Imprimimos por pantalla el array libreria.
        System.out.println(libreria);

        // Eliminamos tres libros del array librería creado anteriormente.
        libreria.remLibro("George Orwell", "1984");
        libreria.remLibro("Alex Huxley", "Un Mundo Feliz");
        libreria.remLibro("Isaac Newton", "Arithmetica Universalis");

        // Imprimimos por pantalla el nuevo array libreria.
        System.out.println(libreria);

        // Imprimimos el precio de los libros eliminados, tiene que salir cero.
        System.out.println(libreria.getPrecioFinal("George Orwell", "1984"));
        System.out.println(libreria.getPrecioFinal("Alex Huxley", "Un Mundo Feliz"));
        System.out.println(libreria.getPrecioFinal("Isaac Newton", "Arithmetica Universalis"));
    }
}