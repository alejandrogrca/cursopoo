package org.uma.mbd.mdLibreriaV2.libreria;

/**
 * Clase Libro
 * @author AlejandroGrca
 */
public class Libro {

    // Variables de clase.
    private String autor;
    private String titulo;
    private double precioBase;
    private static double IVA = 10;

    /**
     * Método constructor que establece un autor, un título y un precio base.
     * @param au Parámetro para establecer el autor del libro.
     * @param ti Parámetro para establecer el título del libro.
     * @param pb Parámetro para establecer el precio base del título.
     */
    public Libro(String au, String ti, double pb) {
        autor = au;
        titulo = ti;
        precioBase = pb;
    }

    /**
     * Método setter para definir el IVA.
     */
    public static void setIVA(double IVA) {
        Libro.IVA = IVA;
    }

    /**
     * Método getter que nos dice el autor del libro.
     * @return Nos devuelve el autor del libro.
     */
    public String getAutor() {
        return autor;
    }

    /**
     * Método getter que nos dice el título del libro.
     * @return Nos devuelve el título del libro.
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Método getter que nos dice el precio base (sin IVA) del libro.
     * @return Nos devuelve el precio base del libro.
     */
    public double getPrecioBase() {
        return precioBase;
    }

    /**
     * Método getter que nos dice el precio final (IVA incluido) del libro.
     * @return Nos devuelve el precio final del libro.
     */
    public double getPrecioFinal() {
        return precioBase + precioBase * IVA / 100;
    }

    /**
     * Método getter que nos dice el IVA que hemos establecido para aplicar al libro.
     * @return Nos devuelve el IVA.
     */
    public static double getIVA() {
        return IVA;
    }

    /**
     * Método toString que nos muestra la información de un libro dados su autor, título, preciobase e IVA.
     * @return Devuelve el array libros en el formato establecido.
     */
    @Override
    public String toString() {
        return "(" + " Autor:" + autor + ";"
                + " Título:" + titulo + ";"
                + " PrecioBase:" + precioBase + ";"
                + " IVA:" + IVA + "%;"
                + " PrecioFinalConIVA:" + getPrecioFinal() + ")";
    }
}
