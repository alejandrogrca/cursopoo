package org.uma.mbd.mdLibreriaV2.libreria;

/**
 * Clase LibroEnOferta
 * @author AlejandroGrca
 */
public class LibroEnOferta extends Libro {

    // Variables de clase.
    private double descuento;

    /**
     * Método constructor que establece información básica sobre un libro. La información del autor y del título se
     * obtienen de la superclase o clase padre.
     * @param titulo Parámetro que establece el autor del libro.
     * @param pb Parámetro que establece el precio base del libro.
     * @param desc Parámetro que establece el descuento que se aplica al libro.
     */
    public LibroEnOferta(String autor, String titulo, double pb, double desc) {
        super(autor, titulo, pb);
        this.descuento=desc;
    }

    /**
     * Método getter que nos dice el precio final (IVA incluido) del libro. Se le quita el descuento.
     * @return Nos devuelve el precio final del libro.
     */
    @Override // Usamos Override para sobrescribir el método getPrecioFinal de la clase padre.
    public double getPrecioFinal() {
        double px=getPrecioBase() - getPrecioBase() *  descuento /100;
        return px + px * getIVA() / 100;
    }

    /**
     * Método getter que nos dice el descuento que hemos establecido para aplicar al libro
     * @return Nos devuelve el descuento.
     */
    public double getDescuento() {
        return descuento;
    }

    /**
     * Método toString que nos muestra toda la información de los datos de un libro.
     * @return Devuelve los datos de un libro en el formato establecido.
     */
    @Override
    public String toString() {
        return "(" + " Autor:" + getAutor() + ";"
                + " Título:" + getTitulo()  + ";"
                + " PrecioBase:" + getPrecioBase() + ";"
                + " Descuento:" + descuento + ";"
                + " PrecioFinal:" + (getPrecioBase() - getPrecioBase() * descuento /100) + ";"
                + " IVA:" + getIVA() + "%;"
                + " PrecioFinalConIVA:" + getPrecioFinal() + ")";
    }
}
