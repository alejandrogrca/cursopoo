package org.uma.mbd.mdLibreriaV3.libreria;

public interface OfertaFlex {
    double getDescuento(Libro libro);
}
