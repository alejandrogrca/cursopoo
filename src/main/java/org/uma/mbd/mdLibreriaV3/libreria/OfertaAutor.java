package org.uma.mbd.mdLibreriaV3.libreria;

import java.util.Arrays;

/**
 * Clase OfertaAutor
 * @author AlejandroGrca
 */
public abstract class OfertaAutor implements OfertaFlex {

    // Variables de clase.
    private String[] autores;
    private double porDescuento;

    /**
     * Método constructor que establece información básica sobre un libro. La información del autor y del título se
     * obtienen de la superclase o clase padre.
     * @param desc Parámetro que establece el descuento que se aplica al libro.
     * @param array Array que establece los autores que tienen descuento.
     */
    public OfertaAutor(double desc, String[] array) {
        porDescuento = desc;
        autores = array;
    }

    /**
     * Método getter que nos dice el descuento de un libro en concreto. Si el autor no se encuentra en el array es
     * AutorenOferta entonces devuelve un descuento de cero.
     * @return Nos devuelve el precio final del libro.
     */
    public double getDescuento(Libro libro) {
        return esAutorenOferta(libro.getAutor()) ? porDescuento : 0;
    }

    /**
     * Método getter complementario al método anterior. Realizamos este método booleano para saber si un autor pertenece
     * al array autores o no.
     * AutorenOferta entonces devuelve un descuento de cero.
     * @return Nos devuelve el precio final del libro.
     */
    private boolean esAutorenOferta(String autor) {
        int i=0;
        while(i < autores.length && ! autor.equalsIgnoreCase(autores[i])) {
            i++;
        }
        return i < autores.length;
    }

    /**
     * Método toString que nos muestra el descuento aplicado y el array de autores con descuento.
     * @return Devuelve un string con el formato establecido.
     */
    @Override
    public String toString() {
        return porDescuento + "% " + Arrays.toString(autores);
    }
}
