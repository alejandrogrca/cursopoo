package org.uma.mbd.mdLibreriaV3.libreria;

/**
 * Clase LibreriaOfertaFlexible
 * @author AlejandroGrca
 */
public class LibreriaOfertaFlexible extends Libreria{

    // Variables de clase.
    private OfertaFlex ofertaFlexible;

    public LibreriaOfertaFlexible(int tam, OfertaFlex oferta) { // Este constructor llama al constructor de la superclase.
        super(tam);
        ofertaFlexible = oferta;
    }

    /**
     * Método constructor que establece un descuento para una serie de autores. El tamaño del array de autores se establece
     * por defecto mediante el parámetro que se definió en la clase padre Libreria (TAM_DEFECTO).
     * @param oferta Objeto que implementa la interfaz OfertaFlex.
     */
    public LibreriaOfertaFlexible(OfertaFlex oferta) { // Este constructor llama al constructor de arriba  (LibreriaOfertaFlexible).
        this(TAM_DEFECTO, oferta);
    }

    /**
     * Método setter que nos crea un nuevo objeto. Este objeto será de tipo LibroEnOferta cuando el autor se encuentre en
     * el array oferta. Si no fuera así entonces crearía un nuevo objeto de tipo Libro.
     */
    @Override // Usamos el Override para sobrescribir el método addLibro de la clase padre Libreria.
    public void addLibro(String autor, String titulo, double precioBase) {
        Libro libro = new Libro(autor, titulo, precioBase);
        double descuento = ofertaFlexible.getDescuento(libro);
        if(descuento > 0) {
            libro = new LibroEnOferta(autor, titulo, precioBase, descuento);
        }
        addLibro(libro);
    }

    /**
     * Método toString que nos muestra la oferta flexible más el toString de la clase padre (Libreria).
     * @return Devuelve el array libros en el formato establecido.
     */
    @Override
    public String toString() {
        return ofertaFlexible + super.toString();
    }
}
