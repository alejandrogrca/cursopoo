package org.uma.mbd.mdLibreriaV3.libreria;

/**
 * Clase LibroEnOferta
 * @author AlejandroGrca
 */
public class OfertaPrecio implements OfertaFlex {

    // Variables de clase
    private double umbralPrecio;
    private double porcDescuento;

    /**
     * Método constructor que establece el descuento y el umbral a aplicar a un libro.
     * @param desc Parámetro que establece el descuento que se aplica al libro.
     * @param umbral Parámetro que establece el umbral a partir del cual se establece el descuento.
     */
    public OfertaPrecio(double desc, double umbral) {
        umbralPrecio = umbral;
        porcDescuento = desc;
    }

    /**
     * Método getter que nos dice el descuento de un libro en concreto. Si el precio base no supera el umbral entonces,
     * se aplica el descuento (porDescuento), sino, el descuento aplicado es cero.
     * @return Nos devuelve el precio final del libro.
     */
    public double getDescuento(Libro libro) {
        return libro.getPrecioBase() >= umbralPrecio ? porcDescuento : 0; // Si se cumple la condición entonces porcDescuento sino 0
    }

    /**
     * Método toString que nos muestra el descuento aplicado más el umbral del precio.
     * @return Devuelve un string con el formato establecido.
     */
    @Override
    public String toString() {
        return porcDescuento + "% (" + umbralPrecio + ")";
    }
}
