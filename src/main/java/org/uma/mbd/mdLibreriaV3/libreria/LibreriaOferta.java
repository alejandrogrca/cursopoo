package org.uma.mbd.mdLibreriaV3.libreria;

import java.util.Arrays;

/**
 * Clase LibreriaOferta
 * @author AlejandroGrca
 */
class LibreriaOferta extends Libreria {

    // Varibles de clase.
    private String[] oferta; // Array de autores que van a tener descuento.
    private double descuento;

    /**
     * Método constructor que establece un descuento para una serie de autores. El tamaño del array de autores se establece
     * por defecto mediante el parámetro que se definió en la clase padre Libreria (TAM_DEFECTO).
     * @param desc Parámetro que establece el descuento a aplicar a cada autor (%).
     * @param aut Parámetro que establece un array de autores que van a tener descuento.
     */
    public LibreriaOferta(double desc, String[] aut) {
        this(TAM_DEFECTO, desc, aut);
    }

    /**
     * Método constructor que establece un descuento para una serie de autores. El tamaño del array de autores se establece
     * mediante un parámetro que le pasamos al método constructor.
     * @param tam Parámetro que establece el tamaño del array de autores.
     * @param desc Parámetro que establece el descuento a aplicar a cada autor(%).
     * @param aut Parámetro que establece un array de autores que van a tener descuento.
     */
    public LibreriaOferta(int tam, double desc, String[] aut) {
        super(tam);
        descuento = desc;
        oferta = aut;
    }

    /**
     * Método setter que actualiza el valor del descuento(%) y de los autores a los que se le aplica.
     */
    public void setOferta(double desc, String[] aut) {
        descuento = desc;
        oferta = aut;
    }

    /**
     * Método getter que nos dice si un autor se encuentra en el array oferta o no. Método auxiliar para
     * el método addLibro de esta mismma clase.
     * @return Nos true si el autor se encuentra en el array oferta y false si no se encuentra.
     */
    private boolean esAutorEnOferta(String autor) {
        int i = 0;
        while (i<oferta.length && ! autor.equalsIgnoreCase(oferta[i])) {
            i++;
        }
        return i < oferta.length;
    }

    /**
     * Método setter que nos crea un nuevo objeto. Este objeto será de tipo LibroEnOferta cuando el autor se encuentre en
     * el array oferta. Si no fuera así entonces crearía un nuevo objeto de tipo Libro.
     */
    @Override // Usamos el Override para sobrescribir el método addLibro de la clase padre Libreria.
    public void addLibro(String autor, String titulo, double pb) {
        Libro libro = null;
        if (esAutorEnOferta(autor)) {
            libro = new LibroEnOferta(autor, titulo, pb, descuento);
        } else {
            libro = new Libro(autor, titulo, pb);
        }
        addLibro(libro);
        }

    /**
     * Método toString que nos muestra el porcentaje de descuento, el autor en oferta y mediante el toString() de la clase
     * padre nos muestra también el array libros de la clase Libreria.
     * @return Devuelve el array libros en el formato establecido.
     */
    @Override
    public String toString() {
        return descuento + "% " + Arrays.toString(oferta) + super.toString();
    }
}
