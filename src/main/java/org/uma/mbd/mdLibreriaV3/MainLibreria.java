package org.uma.mbd.mdLibreriaV3;

import org.uma.mbd.mdLibreriaV3.libreria.Libreria;
import org.uma.mbd.mdLibreriaV3.libreria.LibreriaOfertaFlexible;
import org.uma.mbd.mdLibreriaV3.libreria.OfertaFlex;
import org.uma.mbd.mdLibreriaV3.libreria.OfertaPrecio;

public class MainLibreria {
    public static void main(String [] args) {
        //String [] autores = {"George Orwell", "Isaac Asimov"};

        //OfertaFlex oferta = new OfertaAutor(20, autores);

        // Instanciamos oferta en la clase OfertaPrecio y le establecemos el tamaño y el descuento.
        OfertaFlex oferta = new OfertaPrecio(5, 4);

        // Instanciamos libreria en la clase LibreriaOfertaFlexible y le establecemos el tamaño y el descuento.
        Libreria libreria = new LibreriaOfertaFlexible(3, oferta);

        // Añadimos libros a la libreria que hemos creado.
        libreria.addLibro("george orwell", "1984", 8.20);
        libreria.addLibro("Philip K. Dick", "¿Sueñan los androides con ovejas eléctricas?", 3.50);
        libreria.addLibro("Isaac Asimov", "Fundación e Imperio", 9.40);
        libreria.addLibro("Ray Bradbury","Fahrenheit 451",7.40);
        libreria.addLibro("Alex Huxley", "Un Mundo Feliz",6.50);
        libreria.addLibro("Isaac Asimov", "La Fundación",7.30);
        libreria.addLibro("William Gibson", "Neuromante", 8.30);
        libreria.addLibro("Isaac Asimov","Segunda Fundación",8.10);
        libreria.addLibro("Isaac Newton", "arithmetica universalis", 7.50);
        libreria.addLibro("George Orwell", "1984", 6.20);
        libreria.addLibro("Isaac Newton", "Arithmetica Universalis", 10.50);

        // Imprimimos por pantalla el array libreria.
        System.out.println(libreria);

        // Eliminamos tres libros del array librería creado anteriormente.
        libreria.remLibro("George Orwell", "1984");
        libreria.remLibro("Alex Huxley", "Un Mundo Feliz");
        libreria.remLibro("Isaac Newton", "Arithmetica Universalis");

        // Imprimimos por pantalla el nuevo array libreria.
        System.out.println(libreria);

        // Imprimimos el precio de los libros eliminados, tiene que salir cero.
        System.out.println(libreria.getPrecioFinal("George Orwell", "1984"));
        System.out.println(libreria.getPrecioFinal("Alex Huxley", "Un Mundo Feliz"));
        System.out.println(libreria.getPrecioFinal("Isaac Newton", "Arithmetica Universalis"));
    }
}