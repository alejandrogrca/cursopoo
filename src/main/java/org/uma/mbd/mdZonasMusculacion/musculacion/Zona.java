package org.uma.mbd.mdZonasMusculacion.musculacion;

import java.util.Set;
import java.util.TreeSet;

public class Zona implements Comparable<Zona> {

    private int zonaId;
    private String nombre;
    private double latitud;
    private double longitud;
    private String urlImagen;
    private Set<Maquina> maquinas;

    public Zona(int id, String nom) {
        zonaId = id;
        nombre = nom;
        maquinas = new TreeSet<>(); // Creación de conjunto vacío
    }

    public int getZonaId() {
        return zonaId;
    }

    public String getNombre() {
        return nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public Set<Maquina> getMaquinas() {
        return maquinas;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public void agrega(Maquina maq) {
        maquinas.add(maq);
    }

    @Override
    public int compareTo(Zona zona) {
        return Integer.compare(zonaId,zona.zonaId);
    }

    @Override
    public String toString() {
        return "Zona(" + zonaId + ", " + nombre + ")";
    }
}
