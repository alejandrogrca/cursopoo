package org.uma.mbd.mdZonasMusculacion.musculacion;

import com.opencsv.CSVReader;

import java.io.*;
import java.net.URL;
import java.util.*;

public class Musculacion {

    private final static int MAQUINA_ID = 18;
    private final static int MAQUINA_NOMBRE = 19;
    private final static int MAQUINA_URL_ICON = 21;
    private final static int MAQUINA_NIVEL = 20;
    private final static int MAQUINA_FUNCION = 22;
    private final static int MAQUINA_DESCRIPCION = 23;
    private final static int ZONA_ID = 14;
    private final static int ZONA_NOMBRE = 15;
    private final static int ZONA_URL_ICON = 17;
    private final static int ZONA_UBICACION = 2;

    private String ciudad;
    private Map<Integer, Zona> zonas;

    public Musculacion(String ciudad) {
        this.ciudad = ciudad;
        zonas = new TreeMap<>();
    }

    public String getCiudad() {
        return ciudad;
    }

    public void leeDatosLocal(String ficheroCSV) throws IOException {
        try (InputStream in = new FileInputStream(ficheroCSV);
             InputStreamReader isr = new InputStreamReader(in);
             BufferedReader bin = new BufferedReader(isr);
             CSVReader reader = new CSVReader(bin)) {
            leeDatos(reader);
        }
    }

    public void leeDatosUrl(String urlCSV) throws IOException {
        URL url = new URL(urlCSV);
        try (InputStream in = url.openStream();
             InputStreamReader isr = new InputStreamReader(in);
             BufferedReader bin = new BufferedReader(isr);
             CSVReader reader = new CSVReader(bin)) {
            leeDatos(reader);
        }
    }

    private void leeDatos(CSVReader reader) throws IOException {
        reader.readNext(); // Ignoramos la primera linea
        List<String[]> datos = reader.readAll();  // Se leen todos los datos en un array
        // El resultado es una lista de arrays.
        // Cada array contiene todos los tokens de una linea

        for(String[] tokens : datos) { // Foreach por cada array de tokens
            // Leemos los datos que nos interesan de la maquina
            Maquina maquina = tokensAMaquina(tokens);
            // leemos el identificador de la zona
            int zonaId = Integer.parseInt(tokens[ZONA_ID]);
            // Se debe ver si la zona existe.
            // Si existe no se hace nada
            // Si no existe se debe crear con todos sus datos
            // y almacenar en la correspondencia asociada a su id
            Zona zona = zonas.computeIfAbsent(zonaId, id -> tokensAZona(tokens));
            // Por último, se debe agregar la máquina a esta zona
            zona.agrega(maquina);
        }
    }

    private Maquina tokensAMaquina(String [] tokens) {
        int maquinaId = Integer.parseInt(tokens[MAQUINA_ID]);
        String nombre = tokens[MAQUINA_NOMBRE];
        Maquina maquina = new Maquina(maquinaId, nombre);
        String funcion = tokens[MAQUINA_FUNCION];
        String descripcion = tokens[MAQUINA_DESCRIPCION];
        String urlImagen = tokens[MAQUINA_URL_ICON];
        int nivel = Integer.parseInt(tokens [MAQUINA_NIVEL]);
        maquina.setDescripcion(descripcion);
        maquina.setNivel(nivel);
        maquina.setUrlImagen(urlImagen);
        return maquina;
    }

    private Zona tokensAZona(String[] tokens) {
        int zonaId = Integer.parseInt(tokens[ZONA_ID]);
        String nombre = tokens[ZONA_NOMBRE];
        String urlImagen = tokens[ZONA_URL_ICON];
        String posicion = tokens[ZONA_UBICACION];
        String[] pos = posicion.split("[POINT ()]+"); //split
        double longitud = Double.parseDouble(pos[1]);
        double latitud = Double.parseDouble(pos[2]);
        Zona zona = new Zona(zonaId, nombre);
        zona.setLatitud(latitud);
        zona.setLongitud(longitud);
        zona.setUrlImagen(urlImagen);
        return zona;
    }

    /* En la parte de abajo se hace el mismo método de forma más simple
    public Set<Zona> getZonas() {
        Set<Zona> set = new TreeSet<>();
        for(Zona zona : zonas.values()) {
            set.add(zona);
        }
        return set;
    }*/

    public Set<Zona> getZonas() {
        return new TreeSet<>(zonas.values());
    }

    public Set<Maquina> getMaquinasEnZonaId(int zonaId) {
        Zona zona = zonas.get(zonaId);
        zona.getMaquinas();
        return zona == null ? new TreeSet<>() : zona.getMaquinas();
    }

    public Set<Zona> getZonasConMaquinaId(int mkId) {
        Set<Zona> set = new TreeSet<>();
        Maquina ficticia = new Maquina(mkId, "ficticia");// Craemos máquina ficticia par buscarla en el conjunto
        for(Zona zona : zonas.values()) {
            if(zona.getMaquinas().contains(ficticia)) {
                set.add(zona);
            }
        }
        return set;
    }



}
