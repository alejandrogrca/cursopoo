package org.uma.mbd.mdZonasMusculacion.musculacion;

public class Maquina implements Comparable <Maquina> {

    private int maquinaId;
    private String nombre;
    private String funcion;
    private String descripcion;
    private String urlImagen;
    private int nivel;

    public Maquina(int id, String nom) {
        maquinaId = id;
        nombre = nom;
    }

    public int getMaquinaId() {
        return maquinaId;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFuncion() {
        return funcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public int getNivel() {
        return nivel;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    @Override
    public int compareTo(Maquina maq) {
        return Integer.compare(maquinaId, maq.maquinaId);
    }

    @Override
    public String toString() {
        return "Máquina(" + maquinaId + " " + nombre + ")";
    }
}
