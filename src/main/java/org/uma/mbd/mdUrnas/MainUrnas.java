package org.uma.mbd.mdUrnas;

import org.uma.mbd.mdUrna.urnas.Urna;

import java.util.NoSuchElementException;

public class MainUrnas {

    public static void main(String [] args) {
        try {
            int nb = Integer.parseInt(args[0]);
            int nn = Integer.parseInt(args[1]);

            Urna u = new Urna(nb, nn);

            while (u.totalBolas() > 1) {
                Urna.ColorBola b1 = u.extraerBola();
                Urna.ColorBola b2 = u.extraerBola();
                if (b1 == b2) {
                    u.ponerBlanca();
                } else {
                    u.ponerNegra();
                }
            }
            System.out.println(u.extraerBola());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error en Urna " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Los argumentos deben ser numéricos");
        } catch (IllegalArgumentException | NoSuchElementException e) {
            System.out.println("Error : " + e.getMessage());
        } finally {
            System.out.println("Fin de la aplicación");
        }
    }
}
