package org.uma.mbd.mdUrna.urnas;

import java.util.NoSuchElementException;
import java.util.Random;

public class Urna {
    private static Random alea = new Random();
    public enum ColorBola{Blanca, Negra};

    private int blancas;
    private int negras;

    public Urna(int nb, int nn) {
        if (nb < 0 || nn < 0 || nb + nn == 0) {
            throw new IllegalArgumentException("Valores de entrada erroneos");
        }
        blancas = nb;
        negras = nn;
    }

    public int totalBolas() {
        return blancas + negras;
    }

    public void ponerBlanca() {
        blancas++;
    }

    public void ponerNegra() {
        negras++;
    }

    public ColorBola extraerBola() {
        if (totalBolas() == 0) {
            throw new NoSuchElementException("No hay bolas");
        }
        int na = alea.nextInt(totalBolas()) + 1;
        ColorBola bola = null;
        if (na <= blancas) {
            bola = ColorBola.Blanca;
            blancas--;
        } else {
            bola = ColorBola.Negra;
            negras--;
        }
        return bola;
    }

    @Override
    public String toString() {
        return "U("+ blancas + ", " + negras + ")";
    }

}
