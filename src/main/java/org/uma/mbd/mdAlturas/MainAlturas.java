package org.uma.mbd.mdAlturas;

import org.uma.mbd.mdAlturas.alturas.Pais;
import org.uma.mbd.mdAlturas.alturas.Paises;

import java.io.FileNotFoundException;

public class MainAlturas {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");
        System.out.println("Menores que 1.7");
        for (Pais pais : paises.selecciona(p -> p.getAltura() < 1.7)) {
            System.out.println(pais);
        }
        System.out.println("En europa");
        for (Pais pais : paises.selecciona(p -> p.getContinente().equals("Europe"))) {
            System.out.println(pais);
        }
    }
}
