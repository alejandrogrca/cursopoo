package org.uma.mbd.mdAlturas.alturas;

import java.io.FileNotFoundException;
import java.util.*;
import java.io.File;

public class Paises {

    private List<Pais> paises;

    public Paises() {
        paises = new LinkedList<>();
    }

    public void leePaises(String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner(new File(file))) {
            leePaises(sc);
        }
    }

    public void leePaises(Scanner sc)  {
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
           paises.add(stringToPais(linea));
        }
    }

    private Pais stringToPais(String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[,]");
            sc.useLocale(Locale.ENGLISH);
            String n = sc.next();
            String c = sc.next();
            double al = sc.nextDouble();
            return new Pais(n,c,al);
        }
    }

    public List<Pais> selecciona(Seleccion sel) {
        List<Pais> sol = new ArrayList<>();
        for(Pais pais : paises) {
            if (sel.test(pais)) {
                sol.add(pais);
            }
        }
        return sol;
    }
}
