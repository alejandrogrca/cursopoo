package org.uma.mbd.mdAlturasTreeSet.alturas;

import java.util.Comparator;

public class OrdenContinente implements Comparator<Pais> {

    @Override
    public int compare(Pais p1, Pais p2) {
        return p1.getContinente().compareTo(p2.getContinente());
    }
}
