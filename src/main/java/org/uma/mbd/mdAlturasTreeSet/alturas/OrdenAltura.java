package org.uma.mbd.mdAlturasTreeSet.alturas;

import java.util.Comparator;

public class OrdenAltura implements Comparator<Pais> {

    @Override
    public int compare(Pais p1, Pais p2) {
        return Double.compare(p1.getAltura(), p2.getAltura()); // Como es tipo básico no podemos poner compareTo.
    }
}
