package org.uma.mbd.mdAlturasTreeSet.alturas;

public interface Seleccion {
    boolean test(Pais pais);
}
