package org.uma.mbd.mdRelojArena;

import org.uma.mbd.mdRelojArena.reloj.MedidorTiempo;

/**
 * Método Main
 * @author AlejandroGrca
 */
public class MainMedidorTiempo {
    public static void main(String[] args) {

        MedidorTiempo mt = new MedidorTiempo(7, 3);

        System.out.println(mt.toString()); // A(0,7), B(0,3)
        mt.giraDerecho();

        /* A(0,7), B(3,0) --> Como el tiempo restante de A es cero, entonces tt=trA+trB(3)= 3.
         * El siguiente paso es girar reloj derecho, por tanto el resultado final es A(0,7), B(0,3).
         */

        System.out.println(mt.toString());
        mt.giraAmbos();
        System.out.println(mt.toString());
        mt.giraDerecho();
        System.out.println(mt.toString());
        mt.giraIzquierdo();
        System.out.println(mt.toString());
        mt.giraDerecho();
        System.out.println(mt.toString());
    }
}
