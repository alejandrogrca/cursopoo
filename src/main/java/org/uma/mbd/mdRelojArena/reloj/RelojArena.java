package org.uma.mbd.mdRelojArena.reloj;

/**
 * Clase NPI
 * @author AlejandroGrca
 */
public class RelojArena {

    // Variables de clase.
    private final int tiempoTotal;
    private int tiempoRestante;

    /**
     * Método constructor de la clase RelojArena con un parámetro.
     * @param tt Parámetro que nos indica el tiempo total del reloj (parte inferior + parte superior).
     */
    public RelojArena(int tt) {
        tiempoTotal = tt;
        tiempoRestante = 0;
    }

    /**
     * Método setter para girar el reloj, intercambia los minutos de la parte superior e inferior.
     */
    public void gira() {
        tiempoRestante = tiempoTotal - tiempoRestante;
    }

    /**
     * Método setter para hacer que todos los minutos pasen a la parte inferior.
     */
    public void pasaTiempo() {
        tiempoRestante = 0;
    }

    /**
     * Método getter para saber el número de minutos que queda para que toda la arena esté en la parte inferior.
     * @return Nos devuelve el tiempo restante.
     */
    public int getTiempoRestante() {
        return tiempoRestante;
    }

    /**
     * Método setter para hacer que los minutos pasen a la parte inferior. Hacemos la diferencia del tiempo restante
     * de los dos relojes, si es positiva entonces el tiempo restante de r1 es el resultado de la diferencia anterior
     * y el tiemporestante de r2 será cero. En el caso de que el tr de r1 sea mayor que el tiempo restante de r2 entonces
     * ambos relojes tendrán el tiempo restante de cero.
     * @param reloj Objeto de la clae RelojArena.
     */
    public void pasaTiempo(RelojArena reloj) {
        tiempoRestante = tiempoRestante - reloj.tiempoRestante;
        if (tiempoRestante < 0) {
            tiempoRestante = 0;
        }
        reloj.tiempoRestante = 0;
    }

    /**
     * Método toString para devolver el número de minutos superiores e inferiores con el siguiente formato R(Arriba/Abajo).
     * @return Devuelve un String con la información de cada una de las variables.
     */
    @Override
    public String toString() {
        return "R("+ tiempoRestante + ", " + (tiempoTotal - tiempoRestante) + ")";
    }
}
