package org.uma.mbd.mdRelojArena.reloj;

/**
 * Clase MedidorTiempo
 * @author AlejandroGrca
 */
public class MedidorTiempo {

    // Variables de clase.
    private RelojArena izdo, drcho;
    private int tiempoTotal;

    /**
     * Método constructor en el que definimos el tiempo total de dos relojes.
     * @param ttA Tiempo total del reloj A.
     * @param ttB Tiempo total del reloj B.
     */
    public MedidorTiempo(int ttA, int ttB) {
        izdo = new RelojArena(ttA);
        drcho = new RelojArena(ttB);
    }

    /**
     * Método setter para girar el reloj derecho, intercambia los minutos de la parte superior e inferior.
     */
    public void giraDerecho() {
        drcho.gira();
        pasaTiempo();
    }

    /**
     * Método setter para girar el reloj izquierdo, intercambia los minutos de la parte superior e inferior.
     */
    public void giraIzquierdo() {
        izdo.gira();
        pasaTiempo();
    }

    /**
     * Método setter para girar los dos relojes, intercambia los minutos de la parte superior e inferior.
     */
    public void giraAmbos() {
        drcho.gira();
        izdo.gira();
        pasaTiempo();
    }

    /**
     * Método setter que pasa el tiempo de un reloj a otro. Si uno de los relojes tiene toda la arena en la parte inferior,
     * pasamos el tiempo del otro reloj y se incrementa el total. Si ninguno está vacío, miramos el que menos tiempo le reste
     * si es el derecho, el izquierdo pasa tiempo al derecho, y viceversa, en ambos casos incrementamos el tiempo total.
     */
    public void pasaTiempo() {
        if (izdo.getTiempoRestante() == 0) {
            tiempoTotal += drcho.getTiempoRestante();
            drcho.pasaTiempo();
        } else if (drcho.getTiempoRestante() == 0) {
            tiempoTotal += izdo.getTiempoRestante();
            izdo.pasaTiempo();
        } else {
            // ninguno es cero
            int tI = izdo.getTiempoRestante();
            int tD = drcho.getTiempoRestante();
            if (tI < tD) {
                tiempoTotal += izdo.getTiempoRestante();
                drcho.pasaTiempo(izdo);
            } else {
                tiempoTotal += drcho.getTiempoRestante();
                izdo.pasaTiempo(drcho);
            }
        }
    }

    /**
     * Método getter para saber el tiempo transcurrido desde que se creó el medidor.
     * @return Nos devuelve el tiempo total
     */
    public int getTiempoTotal() {
        return tiempoTotal;
    }

    /**
     * Método toString para devolver el número de minutos superiores e inferiores con el siguiente formato R(Arriba/Abajo).
     * @return Devuelve un String con la información de cada una de las variables.
     */
    @Override
    public String toString() {
        return "MT( izdo = " + izdo + ", drcho = " + drcho + "), Tiempo total: " + tiempoTotal;
    }
}
