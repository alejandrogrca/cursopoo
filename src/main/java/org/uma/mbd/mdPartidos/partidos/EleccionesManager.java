package org.uma.mbd.mdPartidos.partidos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class EleccionesManager {
    private String fEntrada;
    private String fSalida;
    private boolean consola;
	private String [] datos;
    private CriterioSeleccion cs;
    private int numEsc;
    private Elecciones elecciones;
    
    public EleccionesManager(Elecciones elecciones) {
    		this.elecciones = elecciones;
    }

    public EleccionesManager setDatos(String [] datos) {
        this.datos = datos;
        return this;
    }

    public EleccionesManager setCriterioSeleccion(CriterioSeleccion cs) {
        this.cs = cs;
        return this;
    }

    public EleccionesManager setNumEsc(int numEsc) {
        this.numEsc = numEsc;
        return this;
    }

    public EleccionesManager setEntrada(String fEntrada) {
        this.fEntrada = fEntrada;
        return this;
    }

    public EleccionesManager setSalida(String fSalida) {
        this.fSalida = fSalida;
        return this;
    }

    public EleccionesManager setConsola(boolean consola) {
        this.consola = consola;
        return this;
    }

    public void build() throws IOException {
        verify();
        if (datos != null) {
            elecciones.leeDatos(datos);
        } else {
            elecciones.leeDatos(fEntrada);
        }
        Map<Partido, Integer> res = elecciones.generaResultados(cs, numEsc);
        if (fSalida != null) {
            elecciones.presentaResultados(fSalida, res);
        }
        if (consola) {
            PrintWriter pw = new PrintWriter(System.out, true);
            elecciones.presentaResultados(pw, res);
        }

    }

    private void verify() {
        if (datos == null && fEntrada == null) {
            throw new EleccionesException("Faltan los datos de entrada");
        }
        if (datos!= null && fEntrada != null) {
            throw new EleccionesException("Demasiadas entradas de datos");
        }
        if (cs == null) {
            throw  new EleccionesException("Falta criterio de selección");
        }
        if (numEsc <= 0) {
            throw new EleccionesException("Número de escaños incorrecto");
        }
        if (!consola && fSalida == null ) {
            throw new EleccionesException("Falta medio de salida de datos");
        }
    }
}
