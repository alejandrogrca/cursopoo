package org.uma.mbd.mdPartidos.partidos;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pacog on 6/7/16.
 */
public class DHontSimple implements CriterioSeleccion {

    private static Set<Token> creaTokens(List<Partido> lp, int numEsc) {
        Set<Token> set = new TreeSet<>();
        for (Partido p : lp) {
            for (int i = 1; i <= numEsc; i++) {
                set.add(new Token(p, p.getVotos()/(double)i ));
            }
        }
        return set;
    }

    @Override
    public Map<Partido, Integer> ejecuta(List<Partido> lp, int numEsc) {
        Set<Token> set = creaTokens(lp, numEsc);
        Set<Token> nSet = Token.seleccionaTokens(set, numEsc);
        return Token.generaResultados(nSet);
    }
}
