package org.uma.mbd.mdPartidos.partidos;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by pacog on 6/7/16.
 */
public class Proporcional implements CriterioSeleccion {

    private static Set<Token> creaTokens(List<Partido> lp, int numEsc) {
        int sum = 0;
        for (Partido partido : lp) {
            sum += partido.getVotos();
        }
        double vpe = sum/numEsc;
        Set<Token> set = new TreeSet<>();
        for (Partido partido : lp) {
            for (int i = 0; i < numEsc; i++) {
                set.add(new Token(partido, partido.getVotos() - vpe * i));
            }
        }
        return set;
    }

    @Override
    public Map<Partido, Integer> ejecuta(List<Partido> lp, int numEsc) {
        Set<Token> set = creaTokens(lp, numEsc);
        Set<Token> nSet = Token.seleccionaTokens(set, numEsc);
        return Token.generaResultados(nSet);
    }
}
