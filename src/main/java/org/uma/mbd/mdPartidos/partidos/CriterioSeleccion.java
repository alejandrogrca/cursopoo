package org.uma.mbd.mdPartidos.partidos;

import java.util.List;
import java.util.Map;

/**
 * Created by pacog on 6/7/16.
 */
public interface CriterioSeleccion {
    Map<Partido, Integer> ejecuta(List<Partido> partidos, int numEsc);
}
