package org.uma.mbd.mdPartidos.partidos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by pacog on 15/4/16.
 */
public class  Elecciones {
    private List<Partido> partidos;

    public void leeDatos(String [] datos) {
		partidos = new ArrayList<>();
		for (String dato: datos) {
			Partido p = stringToPartido(dato);
			partidos.add(p);
		}	
    }

    public void leeDatos(String fEntrada) throws IOException {
        partidos = new ArrayList<>();
        try (Scanner sc = new Scanner(new File(fEntrada))) {
            while (sc.hasNextLine()) {
                Partido p = stringToPartido(sc.nextLine());
                partidos.add(p);
            }
        }
    }

   private static Partido stringToPartido(String dato) {
        try (Scanner sc = new Scanner(dato)) {
            sc.useDelimiter("[,]+");
            String nombre = sc.next();
            int nv = sc.nextInt();
            if (nv < 0) {
                throw new EleccionesException("Numero de votos negativo en " + dato);
            }
            return new Partido(nombre, nv);
        } catch (InputMismatchException e) {
            throw new EleccionesException("Número de votos erróneo " + dato);
        } catch (NoSuchElementException e) {
            throw new EleccionesException("Faltan datos en " + dato);
        }
    }

   public Map<Partido, Integer> generaResultados(CriterioSeleccion cs, int numEsc) {
       return cs.ejecuta(partidos, numEsc);
   }

    public void presentaResultados(String fSalida, Map<Partido, Integer> map) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(fSalida)) {
            presentaResultados(pw, map);
        }
    }

    public void presentaResultados(PrintWriter pw, Map<Partido, Integer> map) {
        for (Partido partido : partidos) {
            int va = map.getOrDefault(partido, 0);
            pw.println(partido + ", " + ((va == 0) ? "Sin representación" : va));
        }
    }
}

