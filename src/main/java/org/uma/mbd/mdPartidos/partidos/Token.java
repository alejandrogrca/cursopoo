package org.uma.mbd.mdPartidos.partidos;

import java.util.*;

/**
 * Created by pacog on 15/4/16.
 */
public class Token implements Comparable<Token> {
    private Partido partido;
    private double ratio;

    /**
     * Crea un token con un partido y un double
     * @param p partido de este token
     * @param t double que determina el ratio
     */
    public Token(Partido p, double t) {
        partido = p;
        ratio = t;
    }

    /**
     * Devuelve el ratio del token
     * @return ratio
     */
    public double getRatio() {
        return ratio;
    }

    /**
     * Devuelve el partido del token
     * @return partido
     */
    public Partido getPartido() {
        return partido;
    }

    @Override
    public int compareTo(Token tk) {
        int res = -Double.compare(ratio, tk.ratio);
        if (res == 0) {
            res = partido.getNombre().compareTo(tk.partido.getNombre());
        }
        return res;

    }


    /**
     * Selecciona los numEsc mayores del conjunto dado. Se supone
     * que el conjunto esta ordenadopor lo que se seleccionan los numEsc primeros
     * @param tks       Conjunto de tokens
     * @param numEsc    Número de escaños
     * @return  los tokes correspondientes a los seleccionados
     */
    public static Set<Token> seleccionaTokens(Set<Token> tks, int numEsc) {
        Set<Token> set = new TreeSet<>();
        Iterator<Token> iter = tks.iterator();
        int i = 0;
        while (i < numEsc && iter.hasNext()) {
            set.add(iter.next());
            i++;
        }
        return set;
    }

    /**
     * genera una correspondencia   que asocia a cada partido el numero de escños
     * @param tks       Conjunto de tokens
     * @return  Correspondencia entre partidos y tokens
     */

    public static Map<Partido, Integer> generaResultados(Set<Token> tks) {
        Map<Partido, Integer> res = new HashMap<>();
        for (Token token : tks) {
            int va = res.getOrDefault(token.getPartido(), 0);
            res.put(token.getPartido(), va + 1);
        }
        return res;
    }

}
