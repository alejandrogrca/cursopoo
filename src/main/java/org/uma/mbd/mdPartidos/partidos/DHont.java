package org.uma.mbd.mdPartidos.partidos;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by pacog on 6/7/16.
 */
public class DHont extends DHontSimple {
    private double minPor;

    public DHont(double mp) {
        if (mp < 0 || mp > 15) {
            throw new EleccionesException("Tanto por ciento no válido " + mp);
        }
        minPor = mp;
    }


    /**
     * Filtra aquellos partidos que tiene mas del mínimo exigido
     * @param lp    Lista de partidos
     * @return
     */
    private List<Partido> filtraPartidos(List<Partido> lp) {
        List<Partido> lista = new ArrayList<>();
        int sum = 0;
        for (Partido  partido : lp) {
            sum += partido.getVotos();
        }
        double vMin = sum * minPor / 100;
        for (Partido partido : lp) {
            if (partido.getVotos() >= vMin) {
                lista.add(partido);
            }
        }
        return lista;
    }

    @Override
    public Map<Partido, Integer> ejecuta(List<Partido> lp, int numEsc) {
        List<Partido> listaFiltrada = filtraPartidos(lp);
        return super.ejecuta(listaFiltrada, numEsc);
    }
}
