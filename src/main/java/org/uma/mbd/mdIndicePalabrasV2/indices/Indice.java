package org.uma.mbd.mdIndicePalabrasV2.indices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;


public abstract class Indice {
	protected List<String> texto;
	
	public Indice() {
		texto = new ArrayList<>();
	}

	public void agregarDesdeFichero(String ficheroFrases) throws FileNotFoundException {
		try (Scanner sc = new Scanner(new File(ficheroFrases))) {
			agregarDesdeScanner(sc);
		}
	}

	public void agregarDesdeScanner(Scanner sc) {
		while (sc.hasNextLine()) {
			agregarLinea(sc.nextLine());
		}
	}
	
	protected Set<String> leeNoSig(String fns) throws FileNotFoundException {
		Set<String> cs;
		try (Scanner sc = new Scanner(new File(fns))) {
			cs =leeNoSig(sc);
		}
		return cs;
	}
	
	protected Set<String> leeNoSig(Scanner sc) {
		Set<String> set = new HashSet<>();
		while (sc.hasNextLine()) {
			try (Scanner scLinea = new Scanner(sc.nextLine())) {
				while (scLinea.hasNext()) {
					set.add(scLinea.next());	
				}
			}
		}
		return set;
	}

	public void agregarLinea(String linea) {
		texto.add(linea);
	}

	public abstract void resolver(String delim, Collection<String> noSig);
	public abstract void presentarIndice(PrintWriter pw);

	public void resolver(String delim, String ficheroNoSig) throws FileNotFoundException {
		Collection<String> noSig = leeNoSig(ficheroNoSig);
		resolver(delim, noSig);
	}

    public void presentarIndice(String f) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(new File(f))) {
            presentarIndice(pw);
        }
    }

    public void presentarIndiceConsola() {
		try (PrintWriter pw = new PrintWriter(System.out, true)) {
			presentarIndice(pw);
		}
	}
}
