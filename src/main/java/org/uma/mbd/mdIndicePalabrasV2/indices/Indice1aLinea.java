package org.uma.mbd.mdIndicePalabrasV2.indices;

import java.io.PrintWriter;
import java.util.*;

public class Indice1aLinea extends Indice {
	private Map<String, Integer> indice;

	public Indice1aLinea() {
		indice = new TreeMap<>();
	}

	public void agregarLinea(String linea) {
		indice.clear();
		super.agregarLinea(linea);
	}

	public void resolver(String delim, Collection<String> noSig) {
		Set<String> palNoSig = new HashSet<>();
		for (String pNS : noSig) {
			palNoSig.add(pNS.toLowerCase());
		}
		int numLinea = 1;
		for (String linea : texto) {
			try (Scanner sc = new Scanner(linea)) {
				sc.useDelimiter(delim);
				while (sc.hasNext()) {
					String palabra = sc.next().toLowerCase();
					if (!palNoSig.contains(palabra)) {
						Integer i = indice.get(palabra);
						if (i == null) {
							indice.put(palabra, numLinea);
						}
					}
				}
				numLinea++;
			}
		}
	}

	public void presentarIndice(PrintWriter pw) {
		for (String palabra : indice.keySet()) {
			pw.println(palabra + "\t" + indice.get(palabra));
		}
	}
}
