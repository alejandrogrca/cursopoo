package org.uma.mbd.mdIndicePalabrasV2.indices;

import java.io.PrintWriter;
import java.util.*;

public class IndicePosicionesEnLineas extends Indice {
	private Map<String, Map<Integer, List<Integer>>> indice;

	public IndicePosicionesEnLineas() {
		indice = new TreeMap<>();
	}

	public void agregarLinea(String linea) {
		indice.clear();
		super.agregarLinea(linea);
	}

	public void resolver(String delim, Collection<String> noSig) {
		Set<String> palNoSig = new HashSet<>();
		for (String pNS : noSig) {
			palNoSig.add(pNS.toLowerCase());
		}
		int numLinea = 1;
		for (String linea : texto) {
			try (Scanner sc = new Scanner(linea)) {
				sc.useDelimiter(delim);
				int numPal = 1;
				while (sc.hasNext()) {
					String palabra = sc.next().toLowerCase();
					if (!palNoSig.contains(palabra)) {
						Map<Integer, List<Integer>> map = indice.get(palabra);
						if (map == null) {
							map = new TreeMap<>();
							indice.put(palabra, map);
						}
						// PD indices.get(palabra) == map
						List<Integer> li = map.get(numLinea);
						if (li == null) {
							li = new ArrayList<Integer>();
							map.put(numLinea, li);
						}
						// PD map.get(numLinea) == li
						li.add(numPal);
					}
					numPal++;
				}
				numLinea++;
			}
		}
	}

	public void presentarIndice(PrintWriter pw) {
		for (String palabra : indice.keySet()) {
			pw.println(palabra);
			Map<Integer, List<Integer>> map = indice.get(palabra);
			for (Integer i : map.keySet()) {
				pw.print("\t" + i + "\t");
				for (Integer pos : map.get(i)) {
					pw.print(pos + ".");
				}
				pw.println();
			}
		}
	}

}
