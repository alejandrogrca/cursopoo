package org.uma.mbd.mdIndicePalabrasV2.indices;

import java.io.PrintWriter;
import java.util.*;

public class IndiceLineas extends Indice {
	private Map<String, Set<Integer>> indice;

	public IndiceLineas() {
		indice = new TreeMap<String, Set<Integer>>();
	}

	public void agregarLinea(String linea) {
		indice.clear();
		super.agregarLinea(linea);
	}

	public void resolver(String delim, Collection<String> noSig) {
		Set<String> palNoSig = new HashSet<>();
		for (String pNS : noSig) {
			palNoSig.add(pNS.toLowerCase());
		}
		int numLinea = 1;
		for (String linea : texto) {
			try (Scanner sc = new Scanner(linea)) {
				sc.useDelimiter(delim);
				while (sc.hasNext()) {
					String palabra = sc.next().toLowerCase();
					if (!palNoSig.contains(palabra)) {
						Set<Integer> li = indice.get(palabra);
						if (li == null) {
							li = new TreeSet<Integer>();
							indice.put(palabra, li);
						}
						// PD indices.get(palabra) == li
						li.add(numLinea);
					}
				}
				numLinea++;
			}
		}
	}

	public void presentarIndice(PrintWriter pw) {
		for (String palabra : indice.keySet()) {
			pw.print(palabra + "\t");
			Set<Integer> li = indice.get(palabra);
			for (Integer i : li) {
				pw.print(i + ".");
			}
			pw.println();
		}
	}
}
