package org.uma.mbd.mdPartidosStream.partidos;

/**
 * Created by pacog on 15/4/16.
 */
public class EleccionesException extends RuntimeException {
    public EleccionesException() {}

    public EleccionesException(String msg) {
        super(msg);
    }
}
