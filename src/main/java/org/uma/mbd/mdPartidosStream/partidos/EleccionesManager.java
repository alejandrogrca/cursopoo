package org.uma.mbd.mdPartidosStream.partidos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class EleccionesManager {
    private String fEntrada;
    private String fSalida;
    private boolean consola;
	private String [] datos;
    private CriterioSeleccion cs;
    private int numEsc;
    private Elecciones elecciones;
    
    public EleccionesManager(Elecciones elecciones) {
    		this.elecciones = elecciones;
    }

    public EleccionesManager setDatos(String [] datos) {
        this.datos = datos;
        return this;
    }

    public EleccionesManager setCriterioSeleccion(CriterioSeleccion cs) {
        this.cs = cs;
        return this;
    }

    public EleccionesManager setNumEsc(int numEsc) {
        this.numEsc = numEsc;
        return this;
    }

    public EleccionesManager setEntrada(String fEntrada) {
        this.fEntrada = fEntrada;
        return this;
    }

    public EleccionesManager setSalida(String fSalida) {
        this.fSalida = fSalida;
        return this;
    }

    public EleccionesManager setConsola(boolean consola) {
        this.consola = consola;
        return this;
    }

    public void build() throws IOException {
        verify();
        if (fEntrada != null) {
        		elecciones.leeDatos(fEntrada);
        } else {
        		elecciones.leeDatos(datos);
        }
        Map<Partido, Integer> map = elecciones.generaResultados(cs, numEsc);
        if (fSalida != null) {
            elecciones.presentaResultados(fSalida, map);
        }
        if (consola) {
            PrintWriter pw = new PrintWriter(System.out, true);
            elecciones.presentaResultados(pw, map);
        }
    }

    private void verify() {
        // Debe haber una entrada
        if (fEntrada == null && datos == null) {
            throw new EleccionesException("Falta entrada de datos");
        }
        // debe haber una única entrada
        if (fEntrada != null && datos != null) {
            throw new EleccionesException("Hay dos entradas de datos");        	
        }
        // Debe haber un criterio de seleccion
        if (cs == null) {
            throw new EleccionesException("falta criterio de seleccion");
        }
        // Debe haber numero de esc
        if (numEsc <= 0) {
            throw new EleccionesException("Faltan escaños");
        }

        // Debe haber al menos una salida
        if (fSalida == null && !consola) {
            throw new EleccionesException("falta salida de datos");
        }
    }
}
