package org.uma.mbd.mdPartidosStream.partidos;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by pacog on 6/7/16.
 */
public class DHont extends DHontSimple {
    private double minPor;

    public DHont(double mp) {
        if (minPor < 0 || minPor > 15) {
            throw new EleccionesException("Minimo porcentaje erroneo " + mp);
        }
        minPor = mp;
    }

    public Map<Partido, Integer> ejecuta(List<Partido> partidos, int numEsc) {
        int nVotos = partidos.stream().mapToInt(Partido::getVotos).sum();
        double minNumVotos = minPor*nVotos/100;
        List<Partido> partidosFiltrados =
                partidos.stream().filter(p -> p.getVotos() >= minNumVotos)
                  .collect(Collectors.toList());
        return super.ejecuta(partidosFiltrados, numEsc);
    }
}
