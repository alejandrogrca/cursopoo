package org.uma.mbd.mdPartidosStream.partidos;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by pacog on 6/7/16.
 */
public class DHontSimple implements CriterioSeleccion {

    private static Stream<Token> tokensPorPartido(Partido p, int numEsc) {
        return IntStream.rangeClosed(1,numEsc).mapToObj(i -> new Token(p, p.getVotos()/i));
    }

    private static Stream<Token> tokens(List<Partido> lp, int numEsc) {
        return lp.stream().flatMap(p -> tokensPorPartido(p, numEsc));
    }

    @Override
    public Map<Partido, Integer> ejecuta(List<Partido> partidos, int numEsc) {
        return tokens(partidos, numEsc).sorted().limit(numEsc)
                .collect(Collectors.groupingBy(Token::getPartido,
                        Collectors.summingInt(x -> 1)));
    }

}
