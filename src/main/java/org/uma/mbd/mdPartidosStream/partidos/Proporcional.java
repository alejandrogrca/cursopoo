package org.uma.mbd.mdPartidosStream.partidos;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by pacog on 6/7/16.
 */
public class Proporcional implements CriterioSeleccion {

    private static Stream<Token> tokensPorPartido(Partido p, int numEsc, double vpe) {
        return IntStream.rangeClosed(1,numEsc).mapToObj(i -> new Token(p, p.getVotos() - vpe*i));
    }

    private static Stream<Token> tokens(List<Partido> lp, int numEsc, double vpe) {
        return lp.stream().flatMap(p -> tokensPorPartido(p, numEsc, vpe));
    }

    @Override
    public Map<Partido, Integer> ejecuta(List<Partido> partidos, int numEsc) {
        double vpe = partidos.stream().mapToInt(Partido::getVotos).sum()/numEsc;
        return tokens(partidos, numEsc, vpe).sorted().limit(numEsc)
                .collect(Collectors.groupingBy(Token::getPartido,
                         Collectors.summingInt(x -> 1)));
    }
}
