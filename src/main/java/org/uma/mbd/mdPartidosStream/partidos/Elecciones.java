package org.uma.mbd.mdPartidosStream.partidos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created by pacog on 15/4/16.
 */
public class Elecciones {
    private List<Partido>partidos;

    public void leeDatos(String [] datos) {
		partidos = Arrays.stream(datos)
				.map(Elecciones::stringToPartido)
				.collect(Collectors.toList());
    }

    public void leeDatos(String fEntrada) throws IOException {
		partidos = Files.lines(Paths.get(fEntrada))
				.map(Elecciones::stringToPartido)
				.collect(Collectors.toList());
    }

    private static Partido stringToPartido(String dato) {
        try (Scanner sc = new Scanner(dato)) {
            sc.useDelimiter("[,]+");
            String nombre = sc.next();
            int nv = sc.nextInt();
            if (nv < 0) {
                throw new EleccionesException("Numero de votos negativo en " + dato);
            }
            return new Partido(nombre, nv);
        } catch (InputMismatchException e) {
            throw new EleccionesException("Número de votos erróneo " + dato);
        } catch (NoSuchElementException e) {
            throw new EleccionesException("Faltan datos en " + dato);
        }
    }

    public Map<Partido, Integer> generaResultados(CriterioSeleccion cs, int numEsc) {
        return cs.ejecuta(partidos, numEsc);
    }

    public void presentaResultados(String fSalida, Map<Partido, Integer> map) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(fSalida)) {
            presentaResultados(pw, map);
        }
    }

    public void presentaResultados(PrintWriter pw, Map<Partido, Integer> map) {
        partidos.forEach(partido -> {
            int va = map.getOrDefault(partido, 0);
            pw.println(partido + ", " + ((va == 0)?"Sin representación": va));
        });
    }
}

