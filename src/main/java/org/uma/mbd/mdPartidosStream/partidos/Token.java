package org.uma.mbd.mdPartidosStream.partidos;

/**
 * Created by pacog on 15/4/16.
 */
public class Token implements Comparable<Token> {
    private Partido partido;
    private double ratio;

    /**
     * Crea un token con un partido y un entero
     * @param p partido de este token
     * @param t double que determina el ratio
     */
    public Token(Partido p, double t) {
        partido = p;
        ratio = t;
    }

    /**
     * Devuelve el ratio del token
     * @return ratio
     */
    public double getRatio() {
        return ratio;
    }

    /**
     * Devuelve el partido del token
     * @return partido
     */
    public Partido getPartido() {
        return partido;
    }

    @Override
    public int compareTo(Token tk) {
        int res = Double.compare(tk.ratio, ratio);
        if (res == 0) {
            res = getPartido().getNombre().compareTo(tk.getPartido().getNombre());
        }
        return res;

    }
 }
