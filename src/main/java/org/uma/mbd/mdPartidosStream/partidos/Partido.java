package org.uma.mbd.mdPartidosStream.partidos;

/**
 * Created by pacog on 15/4/16.
 */
public class Partido {
    private String nombre;
    private int nVotos;

    /**
     * Crea un partido con getNombre y número de votos
     * @param n  getNombre del partido
     * @param v  número de votos obtenidos
     */
    public Partido(String n, int v) {
        nombre = n;
        nVotos = v;
    }

    /**
     * Devuelve el getNombre del partido
     * @return getNombre del partido
     */
    public String getNombre() {
        return  nombre;
    }

    /**
     * Devuelve el número de votos
     * @return  numero de votos
     */
    public int getVotos() {
        return nVotos;
    }

    /**
     * Dos partidos son iguales si lo son sus nombres, independiente
     * de mayúsculas y minúsculas
     * @param obj   EL objeto a comparar con el receptor
     * @return  si son iguales o no
     */
    @Override
    public boolean equals(Object obj) {
        boolean res = obj instanceof Partido;
        Partido partido = res ? (Partido)obj: null;
        return res && nombre.equalsIgnoreCase(partido.nombre);
    }

    /**
     * Valor compatible con equals
     * @return un entero compatible
     */
    @Override
    public int hashCode() {
        return nombre.toUpperCase().hashCode();
    }

    /**
     * Representación de un partido
     * @return una coadena con la representación del partido
     */
    @Override
    public String toString() {
        return nombre + " : " + nVotos;
    }
}


