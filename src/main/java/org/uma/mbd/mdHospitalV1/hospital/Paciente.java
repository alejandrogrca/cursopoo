package org.uma.mbd.mdHospitalV1.hospital;


public class Paciente extends Persona {
	private double altura;
	private double peso;
	private String segSocial;
	private boolean urgente;
	private Medico medico;

	public Paciente(String dni, String nombre, String apellidos, int edad,
			Genero sexo, double a, double p, String ss, boolean urg) {
		super(dni, nombre, apellidos, edad, sexo);
		altura = a;
		peso = p;
		segSocial = ss;
		urgente = urg;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double a) {
		altura = a;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double p) {
		peso = p;
	}

	public double getIndiceMasaCorporal() {
		return getPeso() / Math.pow(getAltura(), 2);
	}

	public String getNumSegSocial() {
		return segSocial;
	}

	public boolean esUrgencia() {
		return urgente;
	}

	public void setEsUrgencia(boolean b) {
		urgente = b;
	}

	public void asignaMedico( Medico med) {
		medico = med;
	}
	
	public Medico atendidoPor() {
		return medico;
	}
	
	public String toString() {
		return super.toString() + " (" + getIndiceMasaCorporal()  +")";
	}
}
