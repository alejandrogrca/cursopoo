package org.uma.mbd.mdHospitalV1.hospital;



public class Hospital {
		private String nombre;
		private String direccion;
		private Departamento [] departamentos;

	public Hospital(String nombre, String direccion, Departamento[] departamentos, int np) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.departamentos = departamentos;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public int getNumDepartamentos() {
		return departamentos.length;
	}
		
	public Departamento getDepartamento(String nom) {
		Departamento dep = null;
		int i = 0;
		while (i < departamentos.length && dep == null) {
			if (departamentos[i].getNombre().equals(nom)) {
				dep = departamentos[i];
			}
			i++;
		}
		return dep;
	}
	
	
	public String toString() {
		String salida = nombre + " : ";
		for (int i = 0 ; i< departamentos.length; i++) {
			salida += departamentos[i].getNombre() + " ";
		}
		return salida;
	}

}
