package org.uma.mbd.mdHospitalV1.hospital;

public class Departamento {
	/**
	 * @author pacog 
	 */
	private String nombre;
	private Medico [] medicos;
	
	public Departamento(String nombre, Medico [] medicos) {
		this.medicos = medicos;
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
		
	public int getNumMedicos() {
		return medicos.length;
	}

	/**
	 * Devuele el medico con dni dado. null si no existe
	 * @param dni dni del medico
	 * @return el medico encontrado
	 */
	public Medico getMedico(String dni) {
		Medico med = null;
		int i = 0;
		while (i < medicos.length && med == null) {
			if (medicos[i].getDni().equals(dni)) {
				med = medicos[i];
			}
			i++;
		}
		return med;
	}
	
	/**
	 * Encuentra si un medico trabaja en este departamento
	 * @param medico  el medico a buscar. Se compara por dni
	 * @return  cierto si el medico esta emn el departamento
	 */
	public boolean trabajaEnDepartamento(Medico medico) {
		boolean encontrado = false;
		int i = 0;
		while (i < medicos.length && !encontrado) {
			encontrado = medicos[i].getDni().equals(medico.getDni());
			i++;
		}
		return encontrado;

	}
	
	public int numMedicos(Categoria cat) {
		int nm = 0;
		for (int i = 0; i < medicos.length; i++) {
			if (medicos[i].getCategoriaProfesional().equals(cat)) {
				nm++;
			}
		}
		return nm;
	}
	
	public String toString() {
		String salida = nombre + "[ ";
		for (int i = 0; i < medicos.length; i++) {
			salida += medicos[i] + " ";
		}
		salida += "]";
		return salida;
	}
}
