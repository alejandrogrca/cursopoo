package ejercicios.extra.datosHTML.GooglePage.googlePage;

import java.io.*;
import java.net.URL;

public class GooglePage {

    public void hacerDescarga() {
        try {
            URL url = new URL("https://www.google.com/?hl=es");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            File archivo = new File("recursos/DatosGooglePage/descargado.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
            String cadena;
            while ((cadena = br.readLine()) != null) {
                bw.write(cadena);
            }
            br.close();
            bw.close();
            System.out.println("Archivo realizado correctamente");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
