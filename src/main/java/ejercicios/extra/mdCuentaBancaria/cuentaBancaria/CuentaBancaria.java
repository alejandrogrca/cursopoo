package ejercicios.extra.mdCuentaBancaria.cuentaBancaria;

/**
 * Clase CuentaBancaria
 * @author AlejandroGrca
 */
public class CuentaBancaria {

    private String titular;
    private double cantidad;

    /**
     * Método constructor para inicializar titular y cuenta
     * @param titular Parámetro para establecer titular de la cuenta
     * @param cantidad Parámetro para establecer el saldo inicial de la cuenta
     */
    public CuentaBancaria(String titular, double cantidad) {
        this.titular=titular;
        if(cantidad < 0) {
            this.cantidad = 0;
        } else {
            this.cantidad = cantidad;
        }
    }

    /**
     * Método constructor para inicializar titular y cuenta
     * @param titular Parámetro para establecer el titular de la cuenta
     */
    public CuentaBancaria(String titular) {
        this.titular=titular;
        this.cantidad=0;
        // this(titular, 0) Otra forma de poner lo anterior
    }

    /**
     * Método getter para obtener el titular de la cuenta
     */
    public String getTitular() {
        return titular;
    }

    /**
     * Método getter para obtener el saldo que hemos establecido en la cuenta
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * Método setter para ingresa dinero en la cuenta,
     * solo si es positivo la cantidad
     *
     * @param cantidad Parámetro para establecer la cantidad que quermeos ingresar en la cuenta
     */
    public void setIngresar (double cantidad){
        if(cantidad > 0) {
            this.cantidad += cantidad;
        } else {
            cantidad=0;
        }
    }

    /**
     * Método setter para retirar una cantidad de la cuenta, si se quedara en negativo se quedaría en cero
     *
     * @param cantidad Parámetro para establecer la cantidad que queremos sacar de la cuenta
     */
    public void setSacar (double cantidad) {
        if(cantidad > 0) {
            double saldo = (this.cantidad =- cantidad);
            if(saldo>0) {
                this.cantidad =- cantidad;
            } else {
                this.cantidad = 0;
            }
        } else {
            this.cantidad = this.cantidad;
        }
    }

    /**
     * Método toString para devolver el titular de la cuenta y el saldo de la misma
     *
     * @return Nos devuelve información referente a la cuenta bancaria
     */
    @Override
    public String toString() {
        return "El titular de la cuenta es " + titular + " y el saldo en la cuenta es de "  + cantidad;
    }







}